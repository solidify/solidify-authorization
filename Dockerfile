ARG IMAGE_SRC
FROM ${IMAGE_SRC}
ARG USERGID=5000
ARG USERUID=5000

RUN apt update && \
    apt install -y wait-for-it && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*


ARG WAR_FILE

COPY ${WAR_FILE} solidify-authorization.war

# User/Group creation
# Add Authorization user
RUN addgroup --system --gid=${USERGID} authorization
RUN useradd -m -g ${USERGID} --uid=${USERUID} authorization --shell /bin/bash


USER authorization