/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuditingUser.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

import ch.unige.solidify.auth.model.AuthConstants;
import ch.unige.solidify.auth.service.OAuthClientDetails;

public class AuditingUser implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    SecurityContext context = SecurityContextHolder.getContext();
    Authentication authentication = context.getAuthentication();
    // Authenticated user
    if (authentication != null) {
      if (authentication.getPrincipal() instanceof OAuth2AuthenticatedPrincipal) {
        OAuth2AuthenticatedPrincipal oauth2Principal = (OAuth2AuthenticatedPrincipal) authentication.getPrincipal();
        return Optional.of(oauth2Principal.getName());
      } else if (authentication.getPrincipal() instanceof OAuthClientDetails) {
        OAuthClientDetails oauth2Client = (OAuthClientDetails) authentication.getPrincipal();
        return Optional.of(oauth2Client.getClientId());
      } else if (authentication.getPrincipal() instanceof String) {
        return Optional.of((String) authentication.getPrincipal());
      }
    }
    // No authentication
    return Optional.of(AuthConstants.AUTH_SERVER);
  }

}
