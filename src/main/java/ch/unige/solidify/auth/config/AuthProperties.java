/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.config;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import ch.unige.solidify.auth.model.OAuth2GrantType;
import ch.unige.solidify.auth.model.UserProperty;

@Component
@ConfigurationProperties(prefix = "auth")
public class AuthProperties {

  @PostConstruct
  private void checkConfiguration() {
    String tokenKeyStorePath = this.getSecurity().getOauth2().getTokenKeyStorePath();
    this.needsToBeSet(tokenKeyStorePath, "token key store path");
    this.needsToBeSet(this.getSecurity().getOauth2().getTokenKeyStorePassword(), "token key store password");
    this.needsToBeSet(this.getSecurity().getOauth2().getTokenKeyPairName(), "token key pair name");
    this.needsToBeSet(this.getSecurity().getOauth2().getTokenDatabasePassword(), "token database password");
    for (final Application application : this.getApplications()) {
      this.needsOneItem(application.getTrustedUsers(),
              "trusted user list with at least one trusted user for application " + application.getName());
    }
    if (tokenKeyStorePath.startsWith("file:") && !new File(tokenKeyStorePath.substring(tokenKeyStorePath.indexOf(':') + 1)).isFile()) {
      throw new IllegalStateException("Missing file " + tokenKeyStorePath);
    }
    final List<String> allTrustedUserNameList = new ArrayList<>();
    for (final Application application : this.getApplications()) {
      allTrustedUserNameList.addAll(application.getTrustedUsers().stream().map(Application.TrustedUser::getName).collect(Collectors.toList()));
    }
    Set<String> allTrustedUserNameSet = new HashSet<>(allTrustedUserNameList);
    if (allTrustedUserNameSet.size() < allTrustedUserNameList.size()) {
      throw new IllegalStateException("Trusted usernames must be unique across all applications");
    }
  }

  private Http http = new Http();
  private Security security = new Security();
  private Test test = new Test();
  private List<Application> applications = new ArrayList<>();

  private List<UserProperty> userPropertiesUpdatedOnLogin = Arrays.asList(UserProperty.FIRSTNAME, UserProperty.LASTNAME, UserProperty.EMAIL);

  public static class Http {
    private String expectedHeadersCharset = "ISO-8859-1";
    private String shibbolethHeadersCharset = "UTF-8";

    public String getExpectedHeadersCharset() {
      return this.expectedHeadersCharset;
    }

    public String getShibbolethHeadersCharset() {
      return this.shibbolethHeadersCharset;
    }

    public void setExpectedHeadersCharset(String expectedHeadersCharset) {
      this.expectedHeadersCharset = expectedHeadersCharset;
    }

    public void setShibbolethHeadersCharset(String shibbolethHeadersCharset) {
      this.shibbolethHeadersCharset = shibbolethHeadersCharset;
    }
  }

  public static class Security {
    private OAuth2 oauth2 = new OAuth2();
    private Cors cors = new Cors();

    public OAuth2 getOauth2() {
      return this.oauth2;
    }

    public void setOauth2(OAuth2 oauth2) {
      this.oauth2 = oauth2;
    }

    public Cors getCors() {
      return this.cors;
    }

    public void setCors(Cors cors) {
      this.cors = cors;
    }

    public static class OAuth2 {
      private String tokenKeyStorePath = "";
      private String tokenKeyStorePassword = "";
      private String tokenKeyPairName = "";
      private String tokenDatabasePassword = "";

      public String getTokenKeyStorePath() {
        return this.tokenKeyStorePath;
      }

      public void setTokenKeyStorePath(String tokenKeyStorePath) {
        this.tokenKeyStorePath = tokenKeyStorePath;
      }

      public String getTokenKeyStorePassword() {
        return this.tokenKeyStorePassword;
      }

      public void setTokenKeyStorePassword(String tokenKeyStorePassword) {
        this.tokenKeyStorePassword = tokenKeyStorePassword;
      }

      public String getTokenKeyPairName() {
        return this.tokenKeyPairName;
      }

      public void setTokenKeyPairName(String tokenKeyPairName) {
        this.tokenKeyPairName = tokenKeyPairName;
      }

      public String getTokenDatabasePassword() {
        return this.tokenDatabasePassword;
      }

      public void setTokenDatabasePassword(String tokenDatabasePassword) {
        this.tokenDatabasePassword = tokenDatabasePassword;
      }
    }

    public static class Cors {
      // #You can send more than one separated by space
      // http://my.domain.com https://my.domain.com http://my.otherdomain.com
      private String allowedDomains = "http://localhost:4300";

      private String allowedMethods = "POST, GET, PATCH, OPTIONS, DELETE";

      private String allowedHeaders = "x-requested-with, authorization, content-type, content-disposition";

      public String getAllowedDomains() {
        return this.allowedDomains;
      }

      public void setAllowedDomains(String allowedDomains) {
        this.allowedDomains = allowedDomains;
      }

      public String getAllowedMethods() {
        return this.allowedMethods;
      }

      public void setAllowedMethods(String allowedMethods) {
        this.allowedMethods = allowedMethods;
      }

      public String getAllowedHeaders() {
        return this.allowedHeaders;
      }

      public void setAllowedHeaders(String allowedHeaders) {
        this.allowedHeaders = allowedHeaders;
      }
    }
  }

  public static class Test {
    public static class User {
      private String name;
      private String token;

      private String tokenHash;
      private String role;
      private String application;
      private String clientId;

      public String getName() {
        return this.name;
      }

      public void setName(final String name) {
        this.name = name;
      }

      public String getToken() {
        return this.token;
      }

      public void setToken(final String token) {
        this.token = token;
      }

      public String getTokenHash() {
        return this.tokenHash;
      }

      public void setTokenHash(final String tokenHash) {
        this.tokenHash = tokenHash;
      }

      public String getRole() {
        return this.role;
      }

      public void setRole(final String role) {
        this.role = role;
      }

      public String getApplication() {
        return this.application;
      }

      public void setApplication(String application) {
        this.application = application;
      }

      public String getClientId() {
        return this.clientId;
      }

      public void setClientId(String clientId) {
        this.clientId = clientId;
      }
    }

    private String homeOrganizationTest = "unige.ch";
    private boolean generateData = false;
    private List<AuthProperties.Test.User> users = new ArrayList<>();

    public boolean isGenerateData() {
      return this.generateData;
    }

    public void setGenerateData(boolean generateData) {
      this.generateData = generateData;
    }

    public List<AuthProperties.Test.User> getUsers() {
      return this.users;
    }

    public void setUsers(final List<AuthProperties.Test.User> users) {
      this.users = users;
    }

    public String getHomeOrganizationTest() {
      return this.homeOrganizationTest;
    }

    public void setHomeOrganizationTest(String homeOrganizationTest) {
      this.homeOrganizationTest = homeOrganizationTest;
    }

  }

  public static class Application {
    public static class Users {
      private String[] root = {};
      private String[] admin = {};

      public String[] getRoot() {
        return this.root;
      }

      public void setRoot(String[] root) {
        this.root = root;
      }

      public String[] getAdmin() {
        return this.admin;
      }

      public void setAdmin(String[] admin) {
        this.admin = admin;
      }
    }

    public static class TrustedUser {
      private String name;
      private String password;

      public String getName() {
        return this.name;
      }

      public void setName(String name) {
        this.name = name;
      }

      public String getPassword() {
        return this.password;
      }

      public void setPassword(String password) {
        this.password = password;
      }
    }

    public static class OAuth2Client {

      private String name;
      private String clientId;
      private String secret;
      private String redirectUri;
      private boolean isMfa;
      private Collection<OAuth2GrantType> grantTypes = new HashSet<>();
      private Integer accessTokenValiditySeconds = 24 * 60 * 60;
      private Integer refreshTokenValiditySeconds = 24 * 60 * 60;

      public OAuth2Client() {
        this.grantTypes.add(OAuth2GrantType.AUTHORIZATION_CODE);
      }

      public String getName() {
        return this.name;
      }

      public void setName(String name) {
        this.name = name;
      }

      public String getClientId() {
        return this.clientId;
      }

      public void setClientId(String clientId) {
        this.clientId = clientId;
      }

      public String getSecret() {
        return this.secret;
      }

      public void setSecret(String secret) {
        this.secret = secret;
      }

      public String getRedirectUri() {
        return this.redirectUri;
      }

      public boolean isMfa() {
        return this.isMfa;
      }

      public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
      }

      public void setIsMfa(boolean isMfa) {
        this.isMfa = isMfa;
      }

      public Collection<OAuth2GrantType> getGrantTypes() {
        return this.grantTypes;
      }

      public void setGrantTypes(Collection<OAuth2GrantType> grantTypes) {
        this.grantTypes = grantTypes;
      }

      public Integer getAccessTokenValiditySeconds() {
        return this.accessTokenValiditySeconds;
      }

      public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
        this.accessTokenValiditySeconds = accessTokenValiditySeconds;
      }

      public Integer getRefreshTokenValiditySeconds() {
        return this.refreshTokenValiditySeconds;
      }

      public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
        this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
      }
    }

    private String name;

    private Users users = new Users();

    private List<Application.TrustedUser> trustedUsers = new ArrayList<>();

    private List<OAuth2Client> oauth2Clients = new ArrayList<>();

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Users getUsers() {
      return this.users;
    }

    public void setUsers(Users users) {
      this.users = users;
    }

    public List<TrustedUser> getTrustedUsers() {
      return this.trustedUsers;
    }

    public void setTrustedUsers(List<TrustedUser> trustedUsers) {
      this.trustedUsers = trustedUsers;
    }

    public List<OAuth2Client> getOauth2Clients() {
      return this.oauth2Clients;
    }

    public void setOauth2Clients(List<OAuth2Client> oauth2Clients) {
      this.oauth2Clients = oauth2Clients;
    }
  }

  public Http getHttp() {
    return this.http;
  }

  public void setHttp(Http http) {
    this.http = http;
  }

  public Security getSecurity() {
    return this.security;
  }

  public void setSecurity(Security security) {
    this.security = security;
  }

  public Test getTest() {
    return this.test;
  }

  public void setTest(Test test) {
    this.test = test;
  }

  public List<Application> getApplications() {
    return this.applications;
  }

  public void setApplications(List<Application> applications) {
    this.applications = applications;
  }

  public List<UserProperty> getUserPropertiesUpdatedOnLogin() {
    return this.userPropertiesUpdatedOnLogin;
  }

  public void setUserPropertiesUpdatedOnLogin(List<UserProperty> userPropertiesUpdatedOnLogin) {
    this.userPropertiesUpdatedOnLogin = userPropertiesUpdatedOnLogin;
  }

  private void needsToBeSet(String item, String itemMessage) {
    if (item.isEmpty()) {
      throw new IllegalStateException("A " + itemMessage + " must be set");
    }
  }

  private void needsOneItem(List<?> list, String itemMessage) {
    if (list == null || list.isEmpty()) {
      throw new IllegalStateException("A " + itemMessage + " must be set");
    }
  }

}
