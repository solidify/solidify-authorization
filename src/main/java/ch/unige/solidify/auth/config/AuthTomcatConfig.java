/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthTomcatConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.config;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * By default Tomcat allows a maximum of 100 headers in a request. Shibboleth can use more than that
 * set the maximum to 1000.
 *
 * @see <a href="http://tomcat.apache.org/tomcat-9.0-doc/config/http.html">Tomcat documentation</a>
 */
@Configuration
public class AuthTomcatConfig {

  @Bean
  public ServletWebServerFactory servletContainer() {
    final TomcatServletWebServerFactory tomcatFactory = new TomcatServletWebServerFactory();
    tomcatFactory.addConnectorCustomizers(connector -> connector.setProperty("maxHeaderCount", "1000"));
    return tomcatFactory;
  }

}
