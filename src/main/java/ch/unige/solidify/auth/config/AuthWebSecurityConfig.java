/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthWebSecurityConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.config;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import ch.unige.solidify.auth.introspector.AuthJwtOpaqueTokenIntrospector;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.AuthConstants;
import ch.unige.solidify.auth.provider.OAuth2ClientAuthenticationProvider;
import ch.unige.solidify.auth.provider.TrustedUserAuthenticationProvider;
import ch.unige.solidify.auth.rest.UrlPath;
import ch.unige.solidify.auth.service.OAuth2ClientDetailsService;
import ch.unige.solidify.auth.service.OAuthClientDetails;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthWebSecurityConfig {

  private final OAuth2ClientAuthenticationProvider oAuth2ClientAuthenticationProvider;
  private final TrustedUserAuthenticationProvider trustedUserAuthenticationProvider;
  private final AuthJwtOpaqueTokenIntrospector authJwtOpaqueTokenIntrospector;

  private final OAuth2ClientDetailsService oAuth2ClientDetailsService;

  public AuthWebSecurityConfig(OAuth2ClientAuthenticationProvider oAuth2ClientAuthenticationProvider,
          TrustedUserAuthenticationProvider trustedUserAuthenticationProvider,
          AuthJwtOpaqueTokenIntrospector authJwtOpaqueTokenIntrospector,
          OAuth2ClientDetailsService oAuth2ClientDetailsService) {
    this.oAuth2ClientAuthenticationProvider = oAuth2ClientAuthenticationProvider;
    this.trustedUserAuthenticationProvider = trustedUserAuthenticationProvider;
    this.authJwtOpaqueTokenIntrospector = authJwtOpaqueTokenIntrospector;
    this.oAuth2ClientDetailsService = oAuth2ClientDetailsService;
  }

  @Configuration
  @Order(-6)
  public class LoginSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
      // @formatter:off
      http
        .requestMatchers()
          .requestMatchers(new AntPathRequestMatcher(UrlPath.AUTH_SHIBLOGIN), new AntPathRequestMatcher(UrlPath.AUTH_MFA_SHIBLOGIN))
        .and()
        .authorizeRequests()
          .anyRequest().permitAll();
       // @formatter:on
    }
  }

  @Configuration
  @Order(-5)
  public class AuthorizeSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
      // @formatter:off
      http
        .requestMatchers()
          .requestMatchers(new AntPathRequestMatcher(UrlPath.AUTH_AUTHORIZE))
        .and()
        .authorizeRequests()
          .antMatchers(UrlPath.AUTH_AUTHORIZE).authenticated()
        .and()
        .httpBasic()
        .and()
        .authenticationProvider(AuthWebSecurityConfig.this.trustedUserAuthenticationProvider)
        .exceptionHandling()
          .authenticationEntryPoint(new AuthorizeAuthenticationEntryPoint());
      // @formatter:on
    }
  }

  @Configuration
  @Order(-4)
  public class TokenSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
      // @formatter:off
      http
        .requestMatchers()
          .requestMatchers(new AntPathRequestMatcher(UrlPath.AUTH_TOKEN))
        .and()
        .authorizeRequests()
          .antMatchers(UrlPath.AUTH_TOKEN).hasAnyAuthority(AuthApplicationRole.TRUSTED_CLIENT.getResId())
          .antMatchers(HttpMethod.OPTIONS, UrlPath.AUTH_TOKEN).permitAll()
        .and()
        .httpBasic()
        .and()
        .anonymous()
          .disable()
        .formLogin()
          .disable()
        .authenticationProvider(AuthWebSecurityConfig.this.oAuth2ClientAuthenticationProvider)
        .sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .csrf()
          .disable()
        .exceptionHandling()
          .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
    // @formatter:on
    }
  }

  @Configuration
  @Order(-3)
  public class TokenAdminSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
      // @formatter:off
      http
        .requestMatchers()
          .requestMatchers(new AntPathRequestMatcher(UrlPath.AUTH_CHECK_TOKEN))
        .and()
          .authorizeRequests()
            .antMatchers(UrlPath.AUTH_CHECK_TOKEN).hasAnyAuthority(AuthApplicationRole.TRUSTED_CLIENT.getResId())
        .and()
        .httpBasic()
        .and()
        .anonymous()
          .disable()
        .formLogin()
          .disable()
        .authenticationProvider(AuthWebSecurityConfig.this.trustedUserAuthenticationProvider)
        .sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .csrf()
          .disable()
        .exceptionHandling()
          .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
    // @formatter:on
    }
  }

  @Configuration
  @Order(-2)
  public class ModuleAndEntityEndpointConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
      // @formatter:off
      http
        .requestMatchers()
          .requestMatchers(new AntPathRequestMatcher(UrlPath.AUTH_USERS + "/**"))
          .requestMatchers(new AntPathRequestMatcher("/"))
        .and()
        .authorizeRequests()
          .antMatchers(UrlPath.AUTH_USERS).authenticated()
          .antMatchers("/").hasAnyAuthority(AuthApplicationRole.TRUSTED_CLIENT.getResId(), AuthApplicationRole.ROOT.getResId())
        .and()
        .sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
          .and()
          .oauth2ResourceServer()
          .opaqueToken()
          .introspector(authJwtOpaqueTokenIntrospector);
    }
    // @formatter:on
  }

  @Configuration
  @Order(-1)
  public class GeneralSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
      // @formatter:off
      http
        .requestMatchers()
          .anyRequest()
        .and()
        .authorizeRequests()
          .antMatchers(AuthConstants.getPublicUrls()).permitAll()
          .anyRequest().hasAnyAuthority(AuthApplicationRole.ADMIN.getResId(), AuthApplicationRole.ROOT.getResId())
        .and()
        .formLogin()
          .disable()
        .authenticationProvider(AuthWebSecurityConfig.this.trustedUserAuthenticationProvider)
        .sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .exceptionHandling()
          .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
      }
    // @formatter:on
  }

  private class AuthorizeAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private final LoginUrlAuthenticationEntryPoint standardEntryPoint = new LoginUrlAuthenticationEntryPoint(UrlPath.AUTH_SHIBLOGIN);
    private final LoginUrlAuthenticationEntryPoint mfaEntryPoint = new LoginUrlAuthenticationEntryPoint(UrlPath.AUTH_MFA_SHIBLOGIN);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {
      final String[] clientIds = request.getParameterMap().get("client_id");
      final String clientId = clientIds != null && clientIds.length > 0 ? clientIds[0] : null;
      OAuthClientDetails oAuth2ClientDetails = null;
      if (clientId != null) {
          oAuth2ClientDetails = (OAuthClientDetails) oAuth2ClientDetailsService.loadClientByClientId(clientId);
      }
      if(oAuth2ClientDetails != null && oAuth2ClientDetails.isMfa()) {
        this.mfaEntryPoint.commence(request, response, authException);
      } else {
        this.standardEntryPoint.commence(request, response, authException);
      }
    }
  }

}
