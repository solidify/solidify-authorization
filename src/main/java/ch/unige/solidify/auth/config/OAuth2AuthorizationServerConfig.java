/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - OAuth2AuthorizationServerConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.lang.Nullable;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.web.servlet.HandlerInterceptor;

import ch.unige.solidify.auth.rest.UrlPath;
import ch.unige.solidify.auth.service.OAuth2ClientDetailsService;
import ch.unige.solidify.auth.service.SolidifyTokenServices;

// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

  private final OAuth2ClientDetailsService oAuth2ClientDetailsService;
  private final SolidifyTokenServices tokenServices;


  public OAuth2AuthorizationServerConfig(OAuth2ClientDetailsService oAuth2ClientDetailsService, SolidifyTokenServices tokenServices) {
    this.oAuth2ClientDetailsService = oAuth2ClientDetailsService;
    this.tokenServices = tokenServices;
  }

  private static class AuthorizationCodeInterceptor implements HandlerInterceptor {
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) {
      if (UrlPath.AUTH_AUTHORIZE.equals(request.getServletPath())) {
        final HttpSession session = request.getSession(false);
        if (session != null) {
          session.invalidate();
        }
      }
    }
  }

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
    endpoints.allowedTokenEndpointRequestMethods(HttpMethod.POST).tokenServices(this.tokenServices)
            .addInterceptor(new AuthorizationCodeInterceptor());
  }

  @Override
  public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
    clients.withClientDetails(this.oAuth2ClientDetailsService);
  }

}
