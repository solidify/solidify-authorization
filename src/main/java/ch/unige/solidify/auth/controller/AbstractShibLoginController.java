/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AbstractShibLoginController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.controller;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.provider.ShibAuthenticationProvider;
import ch.unige.solidify.auth.service.OAuth2ClientDetailsService;

public abstract class AbstractShibLoginController {
  private static final Logger log = LoggerFactory.getLogger(AbstractShibLoginController.class);
  private final ShibAuthenticationProvider shibAuthenticationProvider;
  private final OAuth2ClientDetailsService oAuth2ClientDetailsService;
  private final String expectedHeadersCharset;
  private final String shibbolethHeadersCharset;

  private final boolean isMfa;

  protected AbstractShibLoginController(AuthProperties config, ShibAuthenticationProvider shibAuthenticationProvider,
          OAuth2ClientDetailsService oAuth2ClientDetailsService, boolean isMfa) {
    this.shibAuthenticationProvider = shibAuthenticationProvider;
    this.oAuth2ClientDetailsService = oAuth2ClientDetailsService;
    this.isMfa = isMfa;
    expectedHeadersCharset = config.getHttp().getExpectedHeadersCharset();
    shibbolethHeadersCharset = config.getHttp().getShibbolethHeadersCharset();
  }

  public ResponseEntity<String> shibLogin(String uniqueId, String mail, String surname, String givenName, String homeOrganization,
          HttpServletRequest request, HttpServletResponse response) {

    // Get parameters from HTTP header
    final String headerXFF = request.getHeader("X-FORWARDED-FOR");
    final String ipAddress = headerXFF == null ? request.getRemoteAddr() : request.getRemoteAddr() + " XFF(" + headerXFF + ")";
    uniqueId = recode(uniqueId, uniqueId);
    mail = recode(mail, uniqueId);
    surname = recode(surname, uniqueId);
    givenName = recode(givenName, uniqueId);
    homeOrganization = recode(homeOrganization, uniqueId);
    log.debug("uniqueID : {}, mail : {}, surname : {}, givenName : {}, homeOrganization : {}, headerXFF : {}, ipAddress : {}",
            uniqueId, mail, surname, givenName, homeOrganization, headerXFF, ipAddress);
    if (log.isDebugEnabled()) {
      log.debug("Request cookies : {}", this.getCookiesAsString(Arrays.asList(request.getCookies())));
    }

    // Get saved request
    final DefaultSavedRequest savedRequest = (DefaultSavedRequest) new HttpSessionRequestCache().getRequest(request, response);
    if(savedRequest == null) {
      return buildBadRequest("Shibboleth login succeeded for (" + uniqueId + ")" + " but authorization endpoint should be called first");
    }
    log.debug("Saved request URL : {}", savedRequest);
    if (log.isDebugEnabled()) {
      log.debug("Saved request cookies : {}", this.getCookiesAsString(savedRequest.getCookies()));
    }

    // Get clientId
    final String[] clientIdParameters = savedRequest.getParameterValues("client_id");
    if(clientIdParameters == null || clientIdParameters.length == 0) {
      return buildBadRequest("clientId parameter is missing from saved request");
    }
    if(clientIdParameters.length > 1) {
      return buildBadRequest("Multiple clientId parameters are present in saved request");
    }
    final String clientId = clientIdParameters[0];
    log.debug("clientId : {}", clientId);

    // Get Application
    final Application application = this.oAuth2ClientDetailsService.findApplicationNameByClientId(clientId);
    log.debug("application : {}", application);
    if(application == null) {
      return buildBadRequest("OAuth2 client not associated with a known application");
    }

    // Authenticate user
    final String user = givenName + " " + surname + " [" + uniqueId + "]";
    log.debug("user : {}", user);
    final Authentication auth = this.shibAuthenticationProvider.authenticate(uniqueId, mail, surname, givenName, homeOrganization,
            ipAddress, application.getName(), this.isMfa);

    // Return 303 See Other
    if (auth.isAuthenticated()) {
      final String redirectURL = savedRequest.getRequestURL() + "?" + savedRequest.getQueryString();
      log.info("Redirect to {}", redirectURL);
      final HttpHeaders responseHeaders = new HttpHeaders();
      try {
        responseHeaders.setLocation(new URI(redirectURL));
        return new ResponseEntity<>(responseHeaders, HttpStatus.SEE_OTHER);
      } catch (URISyntaxException e) {
        return buildBadRequest("Invalid redirect URI " + redirectURL);
      }
    } else {
      return buildBadRequest("Authentication failure for (" + user + ")");
    }
  }

  private ResponseEntity<String> buildBadRequest(String errorMessage) {
    log.error(errorMessage);
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  private String recode(String header, String uniqueId) {
    /*
     * Shibboleth attributes are by default UTF-8 encoded. However, depending on the servlet container
     * configuration they are interpreted as ISO-8859-1 values. This causes problems with non-ASCII
     * characters. The solution is to recode attributes. (see
     * https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPAttributeAccess)
     */
    try {
      return new String(header.getBytes(expectedHeadersCharset), shibbolethHeadersCharset);
    } catch (final UnsupportedEncodingException e) {
      log.error("Unsupported encoding during login of {}", uniqueId, e);
      return "Shibboleth login error for " + uniqueId;
    }
  }

  private String getCookiesAsString(List<Cookie> cookieList) {
    String result = "";
    for (Cookie cookie : cookieList) {
      result += "\n" + cookie.getName() + "=" + cookie.getValue();
    }
    return result;
  }

}
