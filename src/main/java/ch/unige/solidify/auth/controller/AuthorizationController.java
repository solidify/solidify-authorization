/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthorizationController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.auth.model.Module;

@RestController
public class AuthorizationController {
  private static final String MODULE_NAME = "AUTH";

  private final Module mod;

  AuthorizationController(ApplicationContext applicationContext) {
    final String userControllerName = applicationContext.getBeanNamesForType(UserController.class)[0];
    UserController userController = (UserController) applicationContext.getBean(userControllerName);

    this.mod = new Module(MODULE_NAME);
    this.mod.add(linkTo(methodOn(this.getClass()).home()).withSelfRel());
    this.mod.add(linkTo(methodOn(userController.getClass()).home()).withRel("resources"));
  }

  @GetMapping
  public HttpEntity<Module> home() {
    return new ResponseEntity<>(mod, HttpStatus.OK);
  }
}
