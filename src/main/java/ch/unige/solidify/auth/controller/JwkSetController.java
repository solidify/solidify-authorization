/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - JwkSetController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.controller;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;

import org.springframework.core.io.ResourceLoader;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;

import ch.unige.solidify.auth.config.AuthProperties;

/*
 * Publish the public key used for token signing.
 * Use the JWK Set format (RFC 7517)
 * See https://stackoverflow.com/questions/53982192/using-spring-security-oauth2-authorization-server-with-kid-and-jwks
 */
// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
@RestController
public class JwkSetController {

  private static final String KEY_ID = "1";
  private final Map<String, Object> jwkSet;

  public JwkSetController(AuthProperties config, ResourceLoader resourceLoader) {
    final String keyStorePath = config.getSecurity().getOauth2().getTokenKeyStorePath();
    final String keyStorePassword = config.getSecurity().getOauth2().getTokenKeyStorePassword();
    final String keyStoreKeyPairName = config.getSecurity().getOauth2().getTokenKeyPairName();

    final KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resourceLoader.getResource(keyStorePath),
            keyStorePassword.toCharArray());
    final KeyPair keyPair = keyStoreKeyFactory.getKeyPair(keyStoreKeyPairName);
    final RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
    final RSAKey key = new RSAKey.Builder(publicKey).keyID(KEY_ID).keyUse(KeyUse.SIGNATURE).build();
    this.jwkSet = new JWKSet(key).toJSONObject();
  }

  @GetMapping("/.well-known/jwks.json")
  public Map<String, Object> getKey() {
    return this.jwkSet;
  }

}
