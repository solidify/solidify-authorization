/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - ShibLoginController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.provider.ShibAuthenticationProvider;
import ch.unige.solidify.auth.rest.AuthActionName;
import ch.unige.solidify.auth.service.OAuth2ClientDetailsService;


@RestController
public class ShibLoginController extends AbstractShibLoginController{
  public ShibLoginController(AuthProperties config, ShibAuthenticationProvider shibAuthenticationProvider,
          OAuth2ClientDetailsService oAuth2ClientDetailsService) {
    super(config, shibAuthenticationProvider, oAuth2ClientDetailsService, false);
  }

  @GetMapping(AuthActionName.SHIBLOGIN)
  public ResponseEntity<String> shibLogin(@RequestHeader("uniqueid") String uniqueId,
          @RequestHeader("mail") String mail,
          @RequestHeader("surname") String surname,
          @RequestHeader("givenname") String givenName,
          @RequestHeader("homeorganization") String homeOrganization,
          HttpServletRequest request, HttpServletResponse response) {
    return super.shibLogin(uniqueId, mail, surname, givenName, homeOrganization, request, response);
  }

}
