/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - UserController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.AuthLoginInfo;
import ch.unige.solidify.auth.model.AuthOrcidInfo;
import ch.unige.solidify.auth.model.AuthUserDto;
import ch.unige.solidify.auth.model.LoginInfo;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.model.entity.UserApplicationRole;
import ch.unige.solidify.auth.model.entity.UserLoginInfo;
import ch.unige.solidify.auth.repository.UserApplicationRoleRepository;
import ch.unige.solidify.auth.repository.UserLoginInfoRepository;
import ch.unige.solidify.auth.rest.UrlPath;
import ch.unige.solidify.auth.service.AuthUserService;
import ch.unige.solidify.auth.service.TokenSetService;

@RestController
@RequestMapping(UrlPath.AUTH_USERS)
public class UserController {
  static final Logger log = LoggerFactory.getLogger(UserController.class);

  private final AuthUserService authUserService;
  private final TokenSetService tokenSetService;
  private final UserLoginInfoRepository userLoginInfoRepository;
  private final UserApplicationRoleRepository userApplicationRoleRepository;

  public UserController(AuthUserService authUserService,
          TokenSetService tokenSetService,
          UserLoginInfoRepository userLoginInfoRepository,
          UserApplicationRoleRepository userApplicationRoleRepository) {
    this.authUserService = authUserService;
    this.tokenSetService = tokenSetService;
    this.userLoginInfoRepository = userLoginInfoRepository;
    this.userApplicationRoleRepository = userApplicationRoleRepository;
  }

  @PreAuthorize("hasAnyAuthority('TRUSTED_CLIENT', 'ROOT')")
  @DeleteMapping(UrlPath.URL_EXTERNAL_UID + UrlPath.DELETE_TOKEN)
  public ResponseEntity<Void> delete(@PathVariable String externalUid) {
    final AuthUser user = this.authUserService.findByExternalUid(externalUid);
    this.tokenSetService.deleteByUser(user);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("hasAnyAuthority('TRUSTED_CLIENT', 'ROOT')")
  @GetMapping(UrlPath.URL_EXTERNAL_UID)
  public ResponseEntity<AuthUserDto> getUserInfo(@PathVariable String externalUid) {
    final AuthUser user = this.authUserService.findByExternalUid(externalUid);
    final Application application = this.authUserService.getApplicationFromToken();
    AuthUserDto authUserDto = this.getAuthUserDto(user, application);
    // first Login
    Optional<UserLoginInfo> firstLogin = this.userLoginInfoRepository.findTopByUser(user);
    if (firstLogin.isPresent()) {
      authUserDto.setFirstLogin(new AuthLoginInfo(firstLogin.get().getLoginInfo()));
    }
    // Last Login
    List<UserLoginInfo> lastLogin = this.userLoginInfoRepository.findLastLoginsByUser(user);
    if (!lastLogin.isEmpty()) {
      authUserDto.setLastLogin(new AuthLoginInfo(lastLogin.get(0).getLoginInfo()));
    }
    return new ResponseEntity<>(authUserDto, HttpStatus.OK);
  }

  @PreAuthorize("@updateRolePermissionService.isAllowedToUpdateRole(#externalUid, #role)")
  @PostMapping(UrlPath.CHANGE_ROLE)
  public ResponseEntity<AuthUserDto> updateRole(@PathVariable String externalUid, @RequestBody String role) {
    // Check user
    AuthUser user = this.authUserService.findByExternalUid(externalUid);
    final Application application = this.authUserService.getApplicationFromToken();
    // Check application role
    try {
      ApplicationRole applicationRole = AuthApplicationRole.getRoleFromName(role);
      // Update application role
      UserApplicationRole userApplicationRole = this.userApplicationRoleRepository.findByUserAndApplication(user, application)
              .orElseThrow(() -> new IllegalArgumentException("User " + externalUid + " has no role in " + application.getName()));
      userApplicationRole.setApplicationRole(applicationRole.getResId());
      this.authUserService.save(user);
    } catch (RuntimeException e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(this.getAuthUserDto(user, application), HttpStatus.OK);
  }

  @PreAuthorize("hasAnyAuthority('TRUSTED_CLIENT', 'ROOT')")
  @PostMapping(UrlPath.CHANGE_ORCID)
  public ResponseEntity<AuthUserDto> updateOrcid(@PathVariable String externalUid, @RequestBody AuthOrcidInfo orcidInfo) {
    // Check user
    AuthUser user = this.authUserService.findByExternalUid(externalUid);
    final Application application = this.authUserService.getApplicationFromToken();
    // Check application role
    try {
      // Update ORCID
      user.setOrcid(orcidInfo.getOrcid());
      user.setVerifiedOrcid(orcidInfo.isVerifiedOrcid());
      this.authUserService.save(user);
    } catch (RuntimeException e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(this.getAuthUserDto(user, application), HttpStatus.OK);
  }

  @PreAuthorize("hasAnyAuthority('TRUSTED_CLIENT', 'ROOT')")
  @GetMapping(UrlPath.LOGIN_INFOS)
  public ResponseEntity<List<LoginInfo>> listLoginInfos(@PathVariable String externalUid) {
    final AuthUser user = this.authUserService.findByExternalUid(externalUid);
    List<LoginInfo> loginInfos = new ArrayList<>();
    for (UserLoginInfo userLoginInfo : this.userLoginInfoRepository.findByUser(user)) {
      loginInfos.add(userLoginInfo.getLoginInfo());
    }
    return new ResponseEntity<>(loginInfos, HttpStatus.OK);
  }

  public UserController home() {
    return this;
  }

  private AuthUserDto getAuthUserDto(AuthUser authUser, Application application) {
    UserApplicationRole userApplicationRole = this.userApplicationRoleRepository.findByUserAndApplication(authUser, application).
            orElseThrow(() -> new IllegalArgumentException("User " + authUser.getExternalUid() + " has no role in " + application.getName()));
    AuthUserDto authUserDto = new AuthUserDto();
    authUserDto.setResId(authUser.getResId());
    authUserDto.setExternalUid(authUser.getExternalUid());
    authUserDto.setEmail(authUser.getEmail());
    authUserDto.setApplicationRole(new AuthApplicationRole(AuthApplicationRole.getRoleFromName(userApplicationRole.getApplicationRole())));
    authUserDto.setFirstName(authUser.getFirstName());
    authUserDto.setLastName(authUser.getLastName());
    authUserDto.setHomeOrganization(authUser.getHomeOrganization());
    authUserDto.setCreationWhen(authUser.getCreationWhen());
    authUserDto.setCreationWho(authUser.getCreationWho());
    authUserDto.setLastUpdateWhen(authUser.getLastUpdateWhen());
    authUserDto.setLastUpdateWho(authUser.getLastUpdateWho());
    authUserDto.setOrcid(authUser.getOrcid());
    authUserDto.setVerifiedOrcid(authUser.isVerifiedOrcid());
    return authUserDto;
  }
}
