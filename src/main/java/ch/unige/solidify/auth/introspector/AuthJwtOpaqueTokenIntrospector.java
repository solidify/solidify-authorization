/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthJwtOpaqueTokenIntrospector.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.introspector;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.DefaultOAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtException;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.provider.endpoint.CheckTokenEndpoint;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionException;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;

@Service
public class AuthJwtOpaqueTokenIntrospector implements OpaqueTokenIntrospector {

  private final JwtDecoder jwtDecoder = new NimbusJwtDecoder(new ParseOnlyJWTProcessor());
  private final CheckTokenEndpoint checkTokenEndpoint;

  public AuthJwtOpaqueTokenIntrospector(CheckTokenEndpoint checkTokenEndpoint) {
    this.checkTokenEndpoint = checkTokenEndpoint;
  }

  private static class ParseOnlyJWTProcessor extends DefaultJWTProcessor<SecurityContext> {
    @Override
    public JWTClaimsSet process(SignedJWT jwt, SecurityContext context) {
      try {
        return jwt.getJWTClaimsSet();
      } catch (final ParseException e) {
        throw new JwtException("Cannot parse JWT token", e);
      }
    }
  }


  @Override
  public OAuth2AuthenticatedPrincipal introspect(String token) {
    this.checkTokenEndpoint.checkToken(token);
    try {
      final Collection<GrantedAuthority> authorities = new ArrayList<>();
      final Jwt jwt = this.jwtDecoder.decode(token);
      for (final String authority : jwt.getClaimAsStringList("authorities")) {
        authorities.add(new SimpleGrantedAuthority(authority));
      }
      return new DefaultOAuth2AuthenticatedPrincipal(jwt.getClaim("user_name"), jwt.getClaims(), authorities);
    } catch (final JwtException e) {
      throw new OAuth2IntrospectionException("Fail to introspect token", e);
    }
  }

}
