/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

public class AuthConstants {
  public static final String AUTH_SERVER = "Authorization server";
  public static final String TOKEN_APPLICATION_PREFIX = "app-";
  public static final String MFA_SCOPE = "auth-mfa";
  public static final String DB_RES_ID = "resId";
  public static final String DB_USER_ID = "userId";
  public static final String DB_APPLICATION_ID = "applicationName";

  public static final String DB_OAUTH2_CLIENT_ID = "oauth2ClientId";
  public static final String DB_APPLICATION_NAME = "name";
  public static final int DB_DATE_LENGTH = 3;
  public static final int DB_BOOLEAN_LENGTH = 5;
  public static final String NO_REPLY_PREFIX = "noreply-";
  public static final String PERMANENT_TEST_DATA_LABEL = "[Permanent Test Data] ";

  private static final String[] PUBLIC_URLS = {
    "/**/info",
    "/.well-known/jwks.json"
  };

 private AuthConstants() {
   throw new IllegalStateException("Utility class");
 }

  public static String[] getPublicUrls() {
    return PUBLIC_URLS.clone();
  }
}
