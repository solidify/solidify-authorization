/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - ShibAuthentication.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class ShibAuthentication implements Authentication {
  private static final long serialVersionUID = -2520578414071809139L;

  private boolean isAuthenticated = false;
  private final ShibUserDetails userDetails;
  private final String applicationName;
  private final boolean isMfa;


  public ShibAuthentication(ShibUserDetails userDetails, String applicationName, boolean isMfa) {
    this.userDetails = userDetails;
    this.applicationName = applicationName;
    this.isMfa = isMfa;
  }

  public boolean isMfa() {
    return isMfa;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.userDetails.getAuthorities();
  }

  @Override
  public Object getCredentials() {
    return null;
  }

  @Override
  public Object getDetails() {
    return this.userDetails;
  }

  @Override
  public String getName() {
    return this.userDetails.getUsername();
  }

  @Override
  public Object getPrincipal() {
    return this.userDetails;
  }

  @Override
  public boolean isAuthenticated() {
    return this.isAuthenticated;
  }

  @Override
  public void setAuthenticated(boolean isAuthenticated) {
    this.isAuthenticated = isAuthenticated;
  }

  public String getApplicationName() {
    return this.applicationName;
  }

  @Override
  public boolean equals(Object another) {

    if (another == this) {
      return true;
    }
    if (another == null) {
      return false;
    }
    if (!(another instanceof ShibAuthentication)) {
      return false;
    }
    final ShibAuthentication auth = (ShibAuthentication) another;
    return Objects.equals(this.userDetails, auth.getPrincipal()) && this.isAuthenticated == auth.isAuthenticated();

  }

  @Override
  public int hashCode() {
    return Objects.hash(this.userDetails, this.isAuthenticated);
  }

  @Override
  public String toString() {
    return new StringBuilder()
            .append("ShibAuthentication [user=")
            .append(this.userDetails)
            .append(", authenticated=")
            .append(this.isAuthenticated)
            .append("]")
            .toString();
  }

}
