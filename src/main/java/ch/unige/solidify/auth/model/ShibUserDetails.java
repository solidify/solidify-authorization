/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - SolidifyUserDetails.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import ch.unige.solidify.auth.model.entity.AuthUser;

public class ShibUserDetails implements UserDetails {
  private static final long serialVersionUID = -5311822467825656121L;

  private final Collection<? extends GrantedAuthority> authorities;
  private final String externalUid;
  private final boolean isAccountNonExpired;
  private final boolean isAccountNonLocked;
  private final boolean isCredentialsNonExpired;
  private final boolean isEnabled;

  public ShibUserDetails(AuthUser user, String applicationRole) {
    this.authorities = Collections.singleton(new SimpleGrantedAuthority(applicationRole));
    this.externalUid = user.getExternalUid();
    this.isAccountNonExpired = user.isAccountNonExpired();
    this.isAccountNonLocked = user.isAccountNonLocked();
    this.isCredentialsNonExpired = user.isCredentialsNonExpired();
    this.isEnabled = user.isEnabled();
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.authorities;
  }

  @Override
  public String getPassword() {
    throw new UnsupportedOperationException("No password support.");
  }

  @Override
  public String getUsername() {
    return this.externalUid;
  }

  @Override
  public boolean isAccountNonExpired() {
    return this.isAccountNonExpired;
  }

  @Override
  public boolean isAccountNonLocked() {
    return this.isAccountNonLocked;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return this.isCredentialsNonExpired;
  }

  @Override
  public boolean isEnabled() {
    return this.isEnabled;
  }

}
