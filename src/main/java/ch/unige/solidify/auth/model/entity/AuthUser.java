/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - User.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model.entity;

import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import ch.unige.solidify.auth.model.LoginInfo;
import ch.unige.solidify.auth.model.OrcidInfo;

@Entity(name = "user")
@EntityListeners({ AuditingEntityListener.class })
public class AuthUser implements OrcidInfo {

  private static final int MAX_HOME_ORGANIZATION_LENGTH = 512;

  @Id
  private String resId;

  @CreatedDate
  private OffsetDateTime creationWhen;

  @LastModifiedDate
  private OffsetDateTime lastUpdateWhen;

  @CreatedBy
  private String creationWho;

  @LastModifiedBy
  private String lastUpdateWho;

  @NotNull
  private Boolean disabled = Boolean.FALSE;

  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String email;

  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String externalUid;

  @NotNull
  @Size(min = 1)
  private String firstName;

  @NotNull
  @Size(min = 1, max = MAX_HOME_ORGANIZATION_LENGTH)
  @Column(length = MAX_HOME_ORGANIZATION_LENGTH)
  private String homeOrganization;

  @NotNull
  @Size(min = 1)
  private String lastName;

  @Pattern(regexp = OrcidInfo.ORCID_PATTERN, message = OrcidInfo.ORCID_MESSAGE)
  private String orcid;

  @NotNull
  private Boolean verifiedOrcid = false;

  @Transient
  private LoginInfoData lastLogin;

  public AuthUser() {
    this.resId = UUID.randomUUID().toString();
  }



  public String getResId() {
    return this.resId;
  }

  public String getExternalUid() {
    return this.externalUid;
  }

  public String getEmail() {
    return this.email;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getHomeOrganization() {
    return this.homeOrganization;
  }

  public String getLastName() {
    return this.lastName;
  }

  public String getFullName() {
    return this.getLastName() + ", " + this.getFirstName();
  }

  @Transient
  public boolean isAccountNonExpired() {
    return !this.disabled;
  }

  @Transient
  public boolean isAccountNonLocked() {
    return this.isEnabled();
  }

  @Transient
  public boolean isCredentialsNonExpired() {
    return this.isAccountNonExpired();
  }

  public boolean isEnabled() {
    return !this.disabled;
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setExternalUid(String externalUid) {
    this.externalUid = externalUid;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setHomeOrganization(String homeOrganization) {
    this.homeOrganization = homeOrganization;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public String toString() {
    return new StringBuilder()
            .append("User [aai=")
            .append(this.externalUid)
            .append(", email=")
            .append(this.email)
            .append("]")
            .toString();
  }

  public OffsetDateTime getCreationWhen() {
    return this.creationWhen;
  }

  public void setCreationWhen(OffsetDateTime creationWhen) {
    this.creationWhen = creationWhen;
  }

  public OffsetDateTime getLastUpdateWhen() {
    return this.lastUpdateWhen;
  }

  public void setLastUpdateWhen(OffsetDateTime lastUpdateWhen) {
    this.lastUpdateWhen = lastUpdateWhen;
  }

  public String getCreationWho() {
    return this.creationWho;
  }

  public void setCreationWho(String creationWho) {
    this.creationWho = creationWho;
  }

  public String getLastUpdateWho() {
    return this.lastUpdateWho;
  }

  public void setLastUpdateWho(String lastUpdateWho) {
    this.lastUpdateWho = lastUpdateWho;
  }

  public LoginInfo getLastLogin() {
    return this.lastLogin;
  }

  public void setLastLogin(LoginInfoData lastLogin) {
    this.lastLogin = lastLogin;
  }

  @Override
  public String getOrcid() {
    return this.orcid;
  }

  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  @Override
  public Boolean isVerifiedOrcid() {
    return this.verifiedOrcid;
  }

  public void setVerifiedOrcid(Boolean verifiedOrcid) {
    this.verifiedOrcid = verifiedOrcid;
  }

}
