/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - LoginInfoData.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model.entity;

import static ch.unige.solidify.auth.model.AuthConstants.DB_DATE_LENGTH;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

import ch.unige.solidify.auth.model.LoginInfo;

@Embeddable
public class LoginInfoData implements LoginInfo {

  private static final int IP_ADDRESS_SIZE = 255;

  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime loginTime;

  @Size(max = IP_ADDRESS_SIZE)
  @Column(length = IP_ADDRESS_SIZE)
  private String loginIpAddress;

  @Override
  public OffsetDateTime getLoginTime() {
    return this.loginTime;
  }

  public void setLoginTime(OffsetDateTime loginTime) {
    this.loginTime = loginTime;
  }

  @Override
  public String getLoginIpAddress() {
    return this.loginIpAddress;
  }

  public void setLoginIpAddress(String loginIpAddress) {
    if (loginIpAddress != null && loginIpAddress.length() > IP_ADDRESS_SIZE) {
      loginIpAddress = loginIpAddress.substring(0, IP_ADDRESS_SIZE);
    }
    this.loginIpAddress = loginIpAddress;
  }

}
