/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - OAuth2Client.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model.entity;

import static ch.unige.solidify.auth.model.AuthConstants.DB_BOOLEAN_LENGTH;
import static ch.unige.solidify.auth.model.AuthConstants.MFA_SCOPE;
import static ch.unige.solidify.auth.model.AuthConstants.TOKEN_APPLICATION_PREFIX;

import java.util.Collection;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ch.unige.solidify.auth.model.AuthConstants;
import ch.unige.solidify.auth.model.OAuth2GrantType;

@Entity
public class OAuth2Client {

  private static final int MAX_REDIRECT_URI_LENGTH = 512;

  @Id
  private String resId;

  public OAuth2Client() {
    this.resId = UUID.randomUUID().toString();
  }

  @NotNull
  private Integer accessTokenValiditySeconds;

  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String clientId;

  @NotNull
  @ElementCollection(fetch = FetchType.EAGER)
  @Enumerated(EnumType.STRING)
  @Column(name = "grantType")
  private Collection<OAuth2GrantType> grantTypes;

  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String name;

  @NotNull
  @Size(min = 1, max = MAX_REDIRECT_URI_LENGTH)
  @Column(length = MAX_REDIRECT_URI_LENGTH)
  private String redirectUri;

  @NotNull
  private Integer refreshTokenValiditySeconds;

  @NotNull
  @Size(min = 1)
  private String secret;

  @OneToOne(targetEntity = Application.class)
  @JoinColumn(name = AuthConstants.DB_APPLICATION_ID, referencedColumnName = AuthConstants.DB_APPLICATION_NAME)
  private Application application;

  @Column(nullable = false, columnDefinition = "varchar(" + DB_BOOLEAN_LENGTH + ") default 'false'")
  private Boolean isMfa = false;

  public String getClientId() {
    return this.clientId;
  }

  public String getName() {
    return this.name;
  }

  public String getRedirectUri() {
    return this.redirectUri;
  }

  public Collection<OAuth2GrantType> getGrantTypes() {
    return this.grantTypes;
  }

  public Integer getAccessTokenValiditySeconds() {
    return this.accessTokenValiditySeconds;
  }

  public Integer getRefreshTokenValiditySeconds() {
    return this.refreshTokenValiditySeconds;
  }

  public String getScope() {
    String scope = TOKEN_APPLICATION_PREFIX + this.application.getName();
    if (this.isMfa) {
      scope += " " + MFA_SCOPE;
    }
    return scope;
  }

  public String getSecret() {
    return this.secret;
  }

  public Application getApplication() {
    return application;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setRedirectUri(String redirectUri) {
    this.redirectUri = redirectUri;
  }

  public void setGrantTypes(Collection<OAuth2GrantType> grantTypes) {
    this.grantTypes = grantTypes;
  }

  public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
    this.accessTokenValiditySeconds = accessTokenValiditySeconds;
  }

  public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
    this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public boolean isMfa() {
    return this.isMfa;
  }

  public void setIsMfa(boolean isMfa) {
    this.isMfa = isMfa;
  }
}
