/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - TokenSet.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model.entity;

import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import ch.unige.solidify.auth.model.AuthConstants;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { AuthConstants.DB_USER_ID, AuthConstants.DB_OAUTH2_CLIENT_ID }) })
@EntityListeners({ AuditingEntityListener.class })
public class TokenSet {

  private static final int DB_TOKEN_LENGTH = 2048;
  private static final int DB_TOKEN_HASH_LENGTH = 64;

  @Id
  private String resId;

  @OneToOne(targetEntity = AuthUser.class)
  @JoinColumn(name = AuthConstants.DB_USER_ID, referencedColumnName = AuthConstants.DB_RES_ID, nullable = false)
  private AuthUser user;

  @OneToOne(targetEntity = OAuth2Client.class)
  @JoinColumn(name = AuthConstants.DB_OAUTH2_CLIENT_ID, referencedColumnName = AuthConstants.DB_RES_ID, nullable = false)
  private OAuth2Client oAuth2Client;

  @CreatedDate
  private OffsetDateTime creationWhen;

  @LastModifiedDate
  private OffsetDateTime lastUpdateWhen;

  @CreatedBy
  private String creationWho;

  @LastModifiedBy
  private String lastUpdateWho;

  @Size(min = 1, max = DB_TOKEN_LENGTH)
  @Column(length = DB_TOKEN_LENGTH)
  private String accessToken;

  @Size(min = 1, max = DB_TOKEN_HASH_LENGTH)
  @Column(length = DB_TOKEN_HASH_LENGTH)
  private String accessTokenHash;

  @Size(min = 1, max = DB_TOKEN_LENGTH)
  @Column(length = DB_TOKEN_LENGTH)
  private String refreshToken;

  @Size(min = 1, max = DB_TOKEN_HASH_LENGTH)
  @Column(length = DB_TOKEN_HASH_LENGTH)
  private String refreshTokenHash;

  public TokenSet() {
    this.resId = UUID.randomUUID().toString();
  }

  public AuthUser getUser() {
    return this.user;
  }

  public void setUser(AuthUser user) {
    this.user = user;
  }

  public String getAccessToken() {
    return this.accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public void setAccessTokenHash(String accessTokenHash) {
    this.accessTokenHash = accessTokenHash;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public void setRefreshTokenHash(String refreshTokenHash) {
    this.refreshTokenHash = refreshTokenHash;
  }

  public OffsetDateTime getCreationWhen() {
    return this.creationWhen;
  }

  public void setCreationWhen(OffsetDateTime creationWhen) {
    this.creationWhen = creationWhen;
  }

  public OffsetDateTime getLastUpdateWhen() {
    return this.lastUpdateWhen;
  }

  public void setLastUpdateWhen(OffsetDateTime lastUpdateWhen) {
    this.lastUpdateWhen = lastUpdateWhen;
  }

  public String getCreationWho() {
    return this.creationWho;
  }

  public void setCreationWho(String creationWho) {
    this.creationWho = creationWho;
  }

  public String getLastUpdateWho() {
    return this.lastUpdateWho;
  }

  public void setLastUpdateWho(String lastUpdateWho) {
    this.lastUpdateWho = lastUpdateWho;
  }

  public OAuth2Client getOAuth2Client() {
    return this.oAuth2Client;
  }

  public void setOAuth2Client(OAuth2Client oAuth2Client) {
    this.oAuth2Client = oAuth2Client;
  }
}
