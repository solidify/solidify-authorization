/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - UserApplicationRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model.entity;

import java.time.OffsetDateTime;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import ch.unige.solidify.auth.model.AuthConstants;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { AuthConstants.DB_USER_ID, AuthConstants.DB_APPLICATION_ID }) })
@EntityListeners(AuditingEntityListener.class)
public class UserApplicationRole {
  @Id
  private String resId;

  public UserApplicationRole() {
    this.resId = UUID.randomUUID().toString();
  }

  @CreatedDate
  private OffsetDateTime creationWhen;

  @LastModifiedDate
  private OffsetDateTime lastUpdateWhen;

  @CreatedBy
  private String creationWho;

  @LastModifiedBy
  private String lastUpdateWho;

  @OneToOne(targetEntity = AuthUser.class)
  @JoinColumn(name = AuthConstants.DB_USER_ID, referencedColumnName = AuthConstants.DB_RES_ID, nullable = false)
  private AuthUser user;

  @OneToOne(targetEntity = Application.class)
  @JoinColumn(name = AuthConstants.DB_APPLICATION_ID, referencedColumnName = AuthConstants.DB_APPLICATION_NAME, nullable = false)
  private Application application;

  @NotNull
  private String applicationRole;

  public String getResId() {
    return this.resId;
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public OffsetDateTime getCreationWhen() {
    return this.creationWhen;
  }

  public void setCreationWhen(OffsetDateTime creationWhen) {
    this.creationWhen = creationWhen;
  }

  public OffsetDateTime getLastUpdateWhen() {
    return this.lastUpdateWhen;
  }

  public void setLastUpdateWhen(OffsetDateTime lastUpdateWhen) {
    this.lastUpdateWhen = lastUpdateWhen;
  }

  public String getCreationWho() {
    return this.creationWho;
  }

  public void setCreationWho(String creationWho) {
    this.creationWho = creationWho;
  }

  public String getLastUpdateWho() {
    return this.lastUpdateWho;
  }

  public void setLastUpdateWho(String lastUpdateWho) {
    this.lastUpdateWho = lastUpdateWho;
  }

  public AuthUser getUser() {
    return this.user;
  }

  public void setUser(AuthUser user) {
    this.user = user;
  }

  public Application getApplication() {
    return this.application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public String getApplicationRole() {
    return this.applicationRole;
  }

  public void setApplicationRole(String applicationRole) {
    this.applicationRole = applicationRole;
  }

}
