/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - UserLoginInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model.entity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.auth.model.AuthConstants;

@Entity
public class UserLoginInfo {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @JsonIgnore
  private long seq;

  @OneToOne(targetEntity = AuthUser.class)
  @JoinColumn(name = AuthConstants.DB_USER_ID, referencedColumnName = AuthConstants.DB_RES_ID)
  private AuthUser user;

  @Embedded
  private LoginInfoData loginInfo = new LoginInfoData();

  private String applicationName;

  public long getSeq() {
    return this.seq;
  }

  public AuthUser getUser() {
    return this.user;
  }

  public void setUser(AuthUser user) {
    this.user = user;
  }

  public LoginInfoData getLoginInfo() {
    return this.loginInfo;
  }

  public void setLoginInfo(LoginInfoData loginInfo) {
    this.loginInfo = loginInfo;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }
}
