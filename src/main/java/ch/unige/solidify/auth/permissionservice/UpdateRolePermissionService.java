/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - UpdateRolePermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.permissionservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.service.AuthUserService;

@Service
public class UpdateRolePermissionService {
  private static final Logger log = LoggerFactory.getLogger(UpdateRolePermissionService.class);

  private final AuthUserService authUserService;

  public UpdateRolePermissionService(AuthUserService authUserService) {
    this.authUserService = authUserService;
  }


  public boolean isAllowedToUpdateRole(String externalUid, String newRole) {
    try {
      AuthApplicationRole.getRoleFromName(newRole);
    } catch (RuntimeException e) {
      log.warn("Unknown role: {}", newRole);
      return false;
    }
    if (AuthApplicationRole.TRUSTED_CLIENT.getResId().equals(newRole)) {
      return false;
    }
    if (this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.USER) ||
            this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.TRUSTED_CLIENT)) {
      return false;
    }
    if (this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.ROOT)) {
      return !(this.authUserService.getCurrentUser().getExternalUid().equals(externalUid));
    }
    if (this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.ADMIN)) {
      final AuthUser beforeUpdateUser = this.authUserService.findByExternalUid(externalUid);
      final String applicationName = this.authUserService.getApplicationFromToken().getName();
      final String beforeUpdateApplicationRole = this.authUserService.getApplicationRole(beforeUpdateUser, applicationName);
      if (beforeUpdateApplicationRole.equals(newRole)) {
        return true;
      } else {
        return !this.authUserService.getCurrentUser().getExternalUid().equals(externalUid)
                && !AuthApplicationRole.ROOT.getResId().equals(beforeUpdateApplicationRole)
                && !AuthApplicationRole.ROOT.getResId().equals(newRole);
      }
    }
    return false;
  }
}
