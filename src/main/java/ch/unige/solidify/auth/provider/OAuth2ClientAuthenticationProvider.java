/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - OAuth2ClientAuthenticationProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.provider;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.stereotype.Component;

import ch.unige.solidify.auth.service.OAuth2ClientDetailsService;

// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
@Component
public class OAuth2ClientAuthenticationProvider implements AuthenticationProvider {

  private final OAuth2ClientDetailsService oauth2ClientDetailsService;

  public OAuth2ClientAuthenticationProvider(OAuth2ClientDetailsService oauth2ClientDetailsService) {
    this.oauth2ClientDetailsService = oauth2ClientDetailsService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) {

    if (!this.supports(authentication.getClass())) {
      throw new BadCredentialsException("Unsupported authentication");
    }

    final ClientDetails clientDetails = this.oauth2ClientDetailsService.loadClientByClientId((String) authentication.getPrincipal());

    if (clientDetails == null) {
      throw new BadCredentialsException("Bad credentials");
    }

    final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    if (!passwordEncoder.matches(authentication.getCredentials().toString(), clientDetails.getClientSecret())) {
      throw new BadCredentialsException("Bad credentials");
    }

    return new UsernamePasswordAuthenticationToken(clientDetails, authentication.getCredentials(), clientDetails.getAuthorities());
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
  }

}
