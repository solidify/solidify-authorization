/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - ShibAuthenticationProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import ch.unige.solidify.auth.model.ShibAuthentication;
import ch.unige.solidify.auth.model.ShibUserDetails;
import ch.unige.solidify.auth.service.ShibUserService;

@Component
public class ShibAuthenticationProvider {

  private static final Logger log = LoggerFactory.getLogger(ShibAuthenticationProvider.class);

  private final ShibUserService shibUserService;

  public ShibAuthenticationProvider(ShibUserService shibUserService) {
    this.shibUserService = shibUserService;
  }

  public Authentication authenticate(String uniqueId, String mail, String surname, String givenName, String homeOrganization,
          String ipAddress, String applicationName, boolean isMfa) {
    final ShibUserDetails userDetails = this.shibUserService.loadOrCreate(uniqueId, mail, surname, givenName, homeOrganization,
            ipAddress, applicationName);
    final Authentication authentication = new ShibAuthentication(userDetails, applicationName, isMfa);
    authentication.setAuthenticated(true);
    SecurityContextHolder.getContext().setAuthentication(authentication);
    log.debug("Shibboleth authentication user: [{}]", uniqueId);
    return authentication;
  }
}
