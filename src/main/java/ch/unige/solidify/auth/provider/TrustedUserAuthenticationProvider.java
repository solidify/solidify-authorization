/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - TrustedUserAuthenticationProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.provider;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.stereotype.Component;

import ch.unige.solidify.auth.service.TrustedUserDetailsService;

@Component
public class TrustedUserAuthenticationProvider extends DaoAuthenticationProvider {

  public TrustedUserAuthenticationProvider(TrustedUserDetailsService trustedUserDetailsService) {
    super.setUserDetailsService(trustedUserDetailsService);
    this.setPasswordEncoder(NoOpPasswordEncoder.getInstance());
  }

  @Override
  public Authentication authenticate(Authentication authentication) {
    if (!this.supports(authentication.getClass())) {
      throw new BadCredentialsException("Unsupported authentication");
    }
    final String userName = authentication.getName();
    authentication = new UsernamePasswordAuthenticationToken(userName, "{noop}" + authentication.getCredentials(),
            this.getUserDetailsService().loadUserByUsername(userName).getAuthorities());
    return super.authenticate(authentication);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
  }
}
