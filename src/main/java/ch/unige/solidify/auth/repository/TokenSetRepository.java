/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - TokenSetRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.model.entity.OAuth2Client;
import ch.unige.solidify.auth.model.entity.TokenSet;

@Repository
public interface TokenSetRepository extends CrudRepository<TokenSet, String> {
  @Query("SELECT ts FROM TokenSet ts WHERE ts.user = :user AND ts.oAuth2Client = :oAuth2Client")
  TokenSet findByUserAndOAuth2Client(AuthUser user, OAuth2Client oAuth2Client);
  TokenSet findByAccessTokenHash(String accessTokenHash);
  TokenSet findByRefreshTokenHash(String refreshTokenHash);
  void deleteByUser(AuthUser user);
}
