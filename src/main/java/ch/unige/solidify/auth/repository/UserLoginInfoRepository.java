/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - UserLoginInfoRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.model.entity.UserLoginInfo;

@Repository
public interface UserLoginInfoRepository extends CrudRepository<UserLoginInfo, Long> {

  List<UserLoginInfo> findByUser(AuthUser user);

  // Get last logins by user
  @Query("SELECT l FROM UserLoginInfo l WHERE l.user = :user ORDER BY l.loginInfo.loginTime DESC")
  List<UserLoginInfo> findLastLoginsByUser(AuthUser user);

  // Get first login by use
  Optional<UserLoginInfo> findTopByUser(AuthUser user);

  void deleteByUser(AuthUser user);
}
