/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthActionName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.rest;

public class AuthActionName {
  public static final String SHIBLOGIN = "shiblogin";
  public static final String MFA_SHIBLOGIN = "mfa-shiblogin";
  public static final String AUTHORIZE = "authorize";
  public static final String TOKEN = "token";
  public static final String CHECK_TOKEN = "check_token";
  public static final String REVOKE_ALL_TOKENS = "revoke-all-tokens";
  public static final String REVOKE_MY_TOKENS = "revoke-my-tokens";

  private AuthActionName() {
    throw new IllegalStateException("Utility class");
  }
}
