/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - UrlPath.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.rest;

public final class UrlPath {
  public static final String AUTH = "/oauth";
  public static final String AUTH_SHIBLOGIN = "/" + AuthActionName.SHIBLOGIN;
  public static final String AUTH_MFA_SHIBLOGIN = "/" + AuthActionName.MFA_SHIBLOGIN;
  public static final String AUTH_AUTHORIZE = AUTH + "/authorize";
  public static final String AUTH_TOKEN = AUTH + "/token";
  public static final String AUTH_CHECK_TOKEN = AUTH + "/check_token";
  public static final String AUTH_USERS = "/users";
  public static final String URL_EXTERNAL_UID = "/{externalUid}";
  public static final String LOGIN_INFOS = URL_EXTERNAL_UID + "/login-infos";
  public static final String CHANGE_ROLE = URL_EXTERNAL_UID + "/change-role";
  public static final String CHANGE_ORCID = URL_EXTERNAL_UID + "/change-orcid";
  public static final String DELETE_TOKEN = "/delete-token";

  private UrlPath() {
    throw new IllegalStateException("Utility class");
  }
}
