/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - ApplicationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.repository.ApplicationRepository;

@Service
public class ApplicationService {
  private final ApplicationRepository applicationRepository;

  public ApplicationService(AuthProperties authProperties, ApplicationRepository applicationRepository) {
    this.applicationRepository = applicationRepository;
    for (AuthProperties.Application application : authProperties.getApplications()) {
      if (!this.applicationRepository.existsById(application.getName())) {
        this.createApplication(application.getName());
      }
    }
  }

  private void createApplication(String applicationName) {
    Application application = new Application();
    application.setName(applicationName);
    this.applicationRepository.save(application);
  }

}
