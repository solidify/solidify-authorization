/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthUserService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import static ch.unige.solidify.auth.model.AuthConstants.TOKEN_APPLICATION_PREFIX;

import java.util.Collection;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;

import com.nimbusds.jose.shaded.json.JSONArray;

import ch.unige.solidify.auth.exception.AuthResourceNotFound;
import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.repository.ApplicationRepository;
import ch.unige.solidify.auth.repository.AuthUserRepository;
import ch.unige.solidify.auth.repository.UserApplicationRoleRepository;

@Service
public class AuthUserService {
  private final AuthUserRepository authUserRepository;

  private final ApplicationRepository applicationRepository;

  private final UserApplicationRoleRepository userApplicationRoleRepository;

  public AuthUserService(AuthUserRepository authUserRepository,
          ApplicationRepository applicationRepository,
          UserApplicationRoleRepository userApplicationRoleRepository){
    this.authUserRepository = authUserRepository;
    this.applicationRepository = applicationRepository;
    this.userApplicationRoleRepository = userApplicationRoleRepository;
  }

  public AuthUser findByExternalUid(String externalUid) {
      return this.authUserRepository.findByExternalUid(externalUid)
              .orElseThrow(() -> new AuthResourceNotFound(this.getUserNotFoundMessage(externalUid)));
  }

  public void save(AuthUser authUser) {
    this.authUserRepository.save(authUser);
  }

  public AuthUser getCurrentUser() {
    final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    if (principal instanceof OAuth2AuthenticatedPrincipal) {
      final String externalUid = ((OAuth2AuthenticatedPrincipal) principal).getName();
      return this.findByExternalUid(externalUid);
    }
    return null;
  }

  public boolean currentUserHasApplicationRole(ApplicationRole applicationRole) {
    final Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    for (GrantedAuthority authority : authorities) {
      if (authority.getAuthority().equals(applicationRole.getResId())) {
        return true;
      }
    }
    return false;
  }

  public String getApplicationRole(AuthUser user, String applicationName) {
    return this.userApplicationRoleRepository.findByUserAndApplicationName(user, applicationName)
            .orElseThrow(() -> new IllegalStateException("User " + user.getExternalUid() + "has no role for application " + applicationName))
            .getApplicationRole();
  }

  public Application getApplicationFromToken() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!(authentication instanceof BearerTokenAuthentication)) {
      throw new BadCredentialsException("Unsupported authentication");
    }
    BearerTokenAuthentication bearer = (BearerTokenAuthentication) authentication;
    JSONArray scopeArray = (JSONArray) bearer.getTokenAttributes().get("scope");
    if (scopeArray == null || scopeArray.isEmpty()) {
      throw new BadCredentialsException("Token is missing scope");
    }
    final String applicationName = this.getApplicationFromScopes(scopeArray);
    return this.applicationRepository.findByName(applicationName)
            .orElseThrow(() -> new BadCredentialsException("Application specified in token not present in database"));
  }

  private String getUserNotFoundMessage(String externalUid) {
    return "User " + externalUid + " not found";
  }

  private String getApplicationFromScopes(JSONArray scopeArray) {
    for(Object scope : scopeArray) {
      if (!(scope instanceof String)) {
        throw new BadCredentialsException("Scope should be a String");
      }
      final String stringScope = (String) scope;
      if (stringScope.substring(0, TOKEN_APPLICATION_PREFIX.length()).equals(TOKEN_APPLICATION_PREFIX)) {
        return stringScope.substring(TOKEN_APPLICATION_PREFIX.length());
      }
    }
    throw new BadCredentialsException("No application scope in token");
  }

}
