/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - InitTestDataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import static ch.unige.solidify.auth.model.AuthConstants.NO_REPLY_PREFIX;
import static ch.unige.solidify.auth.model.AuthConstants.PERMANENT_TEST_DATA_LABEL;
import static ch.unige.solidify.auth.model.AuthTestConstants.EXTERNAL_UID_TEST_SUFFIX;
import static ch.unige.solidify.auth.model.AuthTestConstants.FIRST_NAME_TEST_SUFFIX;
import static ch.unige.solidify.auth.model.AuthTestConstants.LAST_NAME_TEST_SUFFIX;
import static ch.unige.solidify.auth.model.AuthTestConstants.MAIL_TEST_SUFFIX;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.AuthConstants;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.model.entity.OAuth2Client;
import ch.unige.solidify.auth.model.entity.TokenSet;
import ch.unige.solidify.auth.model.entity.UserApplicationRole;
import ch.unige.solidify.auth.repository.AuthUserRepository;
import ch.unige.solidify.auth.repository.OAuth2ClientRepository;
import ch.unige.solidify.auth.repository.TokenSetRepository;
import ch.unige.solidify.auth.repository.UserApplicationRoleRepository;

@Service
public class InitTestDataService {
  private static final Logger log = LoggerFactory.getLogger(InitTestDataService.class);

  private final List<AuthProperties.Test.User> users;
  private final AuthUserRepository userRepository;

  private final UserApplicationRoleRepository userApplicationRoleRepository;
  private final OAuth2ClientRepository oAuth2ClientRepository;
  private final TokenSetRepository tokenSetRepository;
  private final String homeOrganization;

  public InitTestDataService(AuthProperties authProperties, AuthUserRepository authUserRepository,
          UserApplicationRoleRepository userApplicationRoleRepository, OAuth2ClientRepository oAuth2ClientRepository,
          TokenSetRepository tokenSetRepository) {
    this.users = authProperties.getTest().getUsers();
    this.userRepository = authUserRepository;
    this.userApplicationRoleRepository = userApplicationRoleRepository;
    this.oAuth2ClientRepository = oAuth2ClientRepository;
    this.tokenSetRepository = tokenSetRepository;
    this.homeOrganization = authProperties.getTest().getHomeOrganizationTest();
    if (authProperties.getTest().isGenerateData()) {
      this.initTestUsers();
    }
  }

  private void initTestUsers() {
    for (final AuthProperties.Test.User testUser : this.users) {
      this.createTestUser(testUser);
    }
  }

  private void createTestUser(AuthProperties.Test.User user) {
    String name = user.getName().toUpperCase();
    String mail = NO_REPLY_PREFIX + name + MAIL_TEST_SUFFIX;
    ApplicationRole role = AuthApplicationRole.getRoleFromName(user.getRole());
    AuthUser createdUser = null;
    if (!this.userRepository.existsByEmail(mail)) {
      AuthUser newUser = new AuthUser();
      newUser.setResId(user.getName().toLowerCase());
      newUser.setFirstName(PERMANENT_TEST_DATA_LABEL + name + FIRST_NAME_TEST_SUFFIX);
      newUser.setLastName(PERMANENT_TEST_DATA_LABEL + name + LAST_NAME_TEST_SUFFIX);
      newUser.setEmail(mail);
      newUser.setHomeOrganization(this.homeOrganization);
      newUser.setExternalUid(name + EXTERNAL_UID_TEST_SUFFIX);
      newUser.setCreationWho(AuthConstants.AUTH_SERVER);
      newUser.setLastUpdateWho(AuthConstants.AUTH_SERVER);
      OffsetDateTime now = OffsetDateTime.now();
      newUser.setCreationWhen(now);
      newUser.setLastUpdateWhen(now);
      createdUser = this.userRepository.save(newUser);
      log.info("Test user '{}' created", createdUser.getResId());
    } else {
      Optional<AuthUser> authUserOptional = this.userRepository.findById(user.getName());
      if (authUserOptional.isPresent()) {
        createdUser = authUserOptional.get();
      }
    }
    // Check if already the application role has been created
    if (this.userApplicationRoleRepository.findByUserAndApplicationName(createdUser, user.getApplication()).isEmpty()) {
      Application application = new Application();
      application.setName(user.getApplication());
      UserApplicationRole userApplicationRole = new UserApplicationRole();
      userApplicationRole.setUser(createdUser);
      userApplicationRole.setApplicationRole(role.getResId());
      userApplicationRole.setApplication(application);
      this.userApplicationRoleRepository.save(userApplicationRole);
      log.info("Added role in application '{}' for User '{}'", user.getApplication(), createdUser.getResId());
    }

    final OAuth2Client oAuth2Client = this.oAuth2ClientRepository.findByClientId(user.getClientId());
    // check that there is no already a token created for this specific user and application
    if (createdUser != null && this.tokenSetRepository.findByUserAndOAuth2Client(createdUser, oAuth2Client) == null) {
      TokenSet tokenSet = new TokenSet();
      tokenSet.setUser(createdUser);
      tokenSet.setAccessToken(user.getToken());
      tokenSet.setAccessTokenHash(user.getTokenHash());
      tokenSet.setOAuth2Client(oAuth2Client);
      this.tokenSetRepository.save(tokenSet);
    }
  }
}
