/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - OAuth2ClientDetailsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.exception.AuthResourceNotFound;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.model.entity.OAuth2Client;
import ch.unige.solidify.auth.repository.OAuth2ClientRepository;

// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
@Service
@DependsOn("applicationService")
public class OAuth2ClientDetailsService implements ClientDetailsService {
  private static final Logger log = LoggerFactory.getLogger(OAuth2ClientDetailsService.class);

  private final OAuth2ClientRepository oauth2ClientRepository;

  public OAuth2ClientDetailsService(AuthProperties authProperties, OAuth2ClientRepository oauth2ClientRepository) {
    this.oauth2ClientRepository = oauth2ClientRepository;
    for(AuthProperties.Application application : authProperties.getApplications()) {
      this.initDefaultData(application.getOauth2Clients(), application.getName());
    }
  }

  @Override
  public ClientDetails loadClientByClientId(String clientId) {
    final OAuth2Client client = this.oauth2ClientRepository.findByClientId(clientId);
    if (client != null) {
      return new OAuthClientDetails(client);
    }
    throw new AuthResourceNotFound("Unknown client: " + clientId);
  }

  public Application findApplicationNameByClientId(String clientId) {
    return this.oauth2ClientRepository.findApplicationByClientId(clientId);
  }

  private void initDefaultData(List<AuthProperties.Application.OAuth2Client> oauth2Clients, String applicationName) {
    PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    for (AuthProperties.Application.OAuth2Client oAuth2Client : oauth2Clients) {
      OAuth2Client client = new OAuth2Client();
      client.setName(oAuth2Client.getName());
      client.setClientId(oAuth2Client.getClientId());
      client.setSecret(passwordEncoder.encode(oAuth2Client.getSecret()));
      client.setRedirectUri(oAuth2Client.getRedirectUri());
      client.setIsMfa(oAuth2Client.isMfa());
      client.setGrantTypes(oAuth2Client.getGrantTypes());
      client.setAccessTokenValiditySeconds(oAuth2Client.getAccessTokenValiditySeconds());
      client.setRefreshTokenValiditySeconds(oAuth2Client.getRefreshTokenValiditySeconds());
      Application application = new Application();
      application.setName(applicationName);
      client.setApplication(application);
      this.createIfNotExists(client);
    }
  }

  private void createIfNotExists(OAuth2Client client) {
    if (this.oauth2ClientRepository.findByClientId(client.getClientId()) == null) {
      this.oauth2ClientRepository.save(client);
      this.log.info("OAuth2Client '{}' created", client.getName());
    }
  }

}
