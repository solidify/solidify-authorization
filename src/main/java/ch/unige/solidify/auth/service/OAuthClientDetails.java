/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - OAuthClientDetails.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.OAuth2GrantType;
import ch.unige.solidify.auth.model.entity.OAuth2Client;

// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
public class OAuthClientDetails implements ClientDetails, Principal {
  private static final long serialVersionUID = 1055867951617831900L;

  private final int accessTokenValiditySeconds;
  private final int refreshTokenValiditySeconds;
  private final String clientId;
  private final String secret;
  private final String redirectUri;
  private final String scope;
  private final Collection<OAuth2GrantType> grantTypes;

  private final boolean isMfa;

  public OAuthClientDetails(OAuth2Client oAuth2Client) {
    this.accessTokenValiditySeconds = oAuth2Client.getAccessTokenValiditySeconds();
    this.clientId = oAuth2Client.getClientId();
    this.secret = oAuth2Client.getSecret();
    this.refreshTokenValiditySeconds = oAuth2Client.getRefreshTokenValiditySeconds();
    this.redirectUri = oAuth2Client.getRedirectUri();
    this.scope = oAuth2Client.getScope();
    this.grantTypes = oAuth2Client.getGrantTypes();
    this.isMfa = oAuth2Client.isMfa();
  }

  @Override
  public Integer getAccessTokenValiditySeconds() {
    return this.accessTokenValiditySeconds;
  }

  @Override
  public Map<String, Object> getAdditionalInformation() {
    return new HashMap<>();
  }

  @Override
  public Collection<GrantedAuthority> getAuthorities() {
    return Collections.singleton(new SimpleGrantedAuthority(AuthApplicationRole.TRUSTED_CLIENT.getResId()));
  }

  @Override
  public Set<String> getAuthorizedGrantTypes() {
    return this.grantTypes.stream().map(OAuth2GrantType::toString).collect(Collectors.toSet());
  }

  @Override
  public String getClientId() {
    return this.clientId;
  }

  @Override
  public String getClientSecret() {
    return this.secret;
  }

  @Override
  public String getName() {
    return this.clientId;
  }

  @Override
  public Integer getRefreshTokenValiditySeconds() {
    return this.refreshTokenValiditySeconds;
  }

  @Override
  public Set<String> getRegisteredRedirectUri() {
    final Set<String> redirectUriSet = new HashSet<>();
    redirectUriSet.add(this.redirectUri);
    return redirectUriSet;
  }

  @Override
  public Set<String> getResourceIds() {
    return new HashSet<>();
  }

  @Override
  public Set<String> getScope() {
    return Set.of(scope.split("[\\s+]"));
  }

  @Override
  public boolean isAutoApprove(String scope) {
    return true;
  }

  @Override
  public boolean isScoped() {
    return false;
  }

  @Override
  public boolean isSecretRequired() {
    return false;
  }

  public boolean isMfa() {
    return isMfa;
  }
}
