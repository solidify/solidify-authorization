/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - ShibbolethUserDetailsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.AuthConstants;
import ch.unige.solidify.auth.model.ShibUserDetails;
import ch.unige.solidify.auth.model.UserProperty;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.model.entity.UserApplicationRole;
import ch.unige.solidify.auth.model.entity.UserLoginInfo;
import ch.unige.solidify.auth.repository.ApplicationRepository;
import ch.unige.solidify.auth.repository.AuthUserRepository;
import ch.unige.solidify.auth.repository.UserApplicationRoleRepository;
import ch.unige.solidify.auth.repository.UserLoginInfoRepository;

@Service
public class ShibUserService {

  static final Logger log = LoggerFactory.getLogger(ShibUserService.class);

  private final AuthUserRepository authUserRepository;

  private final UserApplicationRoleRepository userApplicationRoleRepository;

  private final UserLoginInfoRepository userLoginInfoRepository;

  private final ApplicationRepository applicationRepository;

  private final List<AuthProperties.Application> applicationsFromConfig;

  private final List<UserProperty> userPropertiesUpdatedOnLogin;

  public ShibUserService(AuthProperties config, AuthUserRepository authUserRepository,
          UserApplicationRoleRepository userApplicationRoleRepository, UserLoginInfoRepository userLoginInfoRepository,
          ApplicationRepository applicationRepository) {
    this.authUserRepository = authUserRepository;
    this.userApplicationRoleRepository = userApplicationRoleRepository;
    this.userLoginInfoRepository = userLoginInfoRepository;
    this.applicationRepository = applicationRepository;
    this.applicationsFromConfig = config.getApplications();
    this.userPropertiesUpdatedOnLogin = config.getUserPropertiesUpdatedOnLogin();
  }

  /**
   * Load and considers the creation of a new user once the Shibboleth layer authentication was done
   */
  @Transactional
  public ShibUserDetails loadOrCreate(String externalUid, String mail, String lastName, String firstName, String homeOrganization,
          String ipAddress, String applicationName) {

    // Create user if absent
    Optional<AuthUser> optionalUser = this.authUserRepository.findByExternalUid(externalUid);
    AuthUser user;
    if (optionalUser.isEmpty()) {
      user = new AuthUser();
      OffsetDateTime now = OffsetDateTime.now();
      user.setExternalUid(externalUid);
      user.setEmail(mail);
      user.setFirstName(firstName);
      user.setLastName(lastName);
      user.setHomeOrganization(homeOrganization);
      user.setCreationWhen(now);
      user.setLastUpdateWhen(now);
      user.setCreationWho(AuthConstants.AUTH_SERVER);
      user.setLastUpdateWho(AuthConstants.AUTH_SERVER);
      user = this.authUserRepository.save(user);
      log.info("Creation of user [ {} ]", user);
    } else {
      user = optionalUser.get();

      if (this.userPropertiesUpdatedOnLogin.contains(UserProperty.FIRSTNAME)) {
        user.setFirstName(firstName);
      }
      if (this.userPropertiesUpdatedOnLogin.contains(UserProperty.LASTNAME)) {
        user.setLastName(lastName);
      }
      if (this.userPropertiesUpdatedOnLogin.contains(UserProperty.EMAIL)) {
        user.setEmail(mail);
      }
      if (this.userPropertiesUpdatedOnLogin.contains(UserProperty.HOME_ORGANIZATION)) {
        user.setHomeOrganization(homeOrganization);
      }
    }

    // Create application role if absent
    Optional<UserApplicationRole> optionalUserApplicationRole = this.userApplicationRoleRepository.findByUserAndApplicationName(user,
            applicationName);
    if (optionalUserApplicationRole.isEmpty()) {
      final AuthProperties.Application applicationFromConfig = this.getApplication(applicationName);
      final Application application = this.applicationRepository.findByName(applicationFromConfig.getName())
              .orElseThrow(() -> new IllegalArgumentException("Application " + applicationFromConfig.getName() + " not registered in database"));
      final ApplicationRole applicationRole = this.getApplicationRoleFromConfig(externalUid, applicationFromConfig);
      final UserApplicationRole userApplicationRole = new UserApplicationRole();
      userApplicationRole.setUser(user);
      userApplicationRole.setApplicationRole(applicationRole.getResId());
      userApplicationRole.setApplication(application);
      this.userApplicationRoleRepository.save(userApplicationRole);
      log.info("Add role {} in {} for {}", applicationRole.getResId(), applicationName, user);
    }

    // Save user login
    UserLoginInfo userLoginInfo = new UserLoginInfo();
    userLoginInfo.setUser(user);
    userLoginInfo.setApplicationName(applicationName);
    userLoginInfo.getLoginInfo().setLoginIpAddress(ipAddress);
    userLoginInfo.getLoginInfo().setLoginTime(OffsetDateTime.now());
    this.userLoginInfoRepository.save(userLoginInfo);
    UserApplicationRole userApplicationRole = this.userApplicationRoleRepository.findByUserAndApplicationName(user, applicationName)
            .orElseThrow(() -> new IllegalArgumentException("User " + externalUid + " has no role in " + applicationName));
    return new ShibUserDetails(user, userApplicationRole.getApplicationRole());
  }

  private AuthProperties.Application getApplication(String applicationName) {
    for (AuthProperties.Application application : this.applicationsFromConfig) {
      if (applicationName.equals(application.getName())) {
        return application;
      }
    }
    throw new IllegalArgumentException("Application " + applicationName + " is not registered in configuration");
  }

  private ApplicationRole getApplicationRoleFromConfig(String externalUid, AuthProperties.Application applicationFromConfig) {
    ApplicationRole applicationRole = null;

    if (Arrays.asList(applicationFromConfig.getUsers().getAdmin()).contains(externalUid)) {
      applicationRole = AuthApplicationRole.ADMIN;
    }

    if (Arrays.asList(applicationFromConfig.getUsers().getRoot()).contains(externalUid)) {
      applicationRole = AuthApplicationRole.ROOT;
    }

    if (applicationRole == null) {
      applicationRole = AuthApplicationRole.USER;
    }
    return applicationRole;
  }
}
