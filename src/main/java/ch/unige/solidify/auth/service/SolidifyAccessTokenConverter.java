/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - SolidifyAccessTokenConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import org.springframework.core.io.ResourceLoader;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.config.AuthProperties;

// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
@Service
public class SolidifyAccessTokenConverter extends JwtAccessTokenConverter {

  public SolidifyAccessTokenConverter(AuthProperties config, ResourceLoader resourceLoader) {
    final String keyStorePath = config.getSecurity().getOauth2().getTokenKeyStorePath();
    final String keyStorePassword = config.getSecurity().getOauth2().getTokenKeyStorePassword();
    final String keyStoreKeyPair = config.getSecurity().getOauth2().getTokenKeyPairName();

    final KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resourceLoader.getResource(keyStorePath),
            keyStorePassword.toCharArray());
    this.setKeyPair(keyStoreKeyFactory.getKeyPair(keyStoreKeyPair));
  }
}
