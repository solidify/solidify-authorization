/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - SolidifyTokenServices.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

/**
 * OAuth2 token services
 * https://github.com/spring-projects/spring-security-oauth/issues/685#issuecomment-371845995
 **/
// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
@Service
public class SolidifyTokenServices extends DefaultTokenServices {

  private SolidifyTokenStore tokenStore;

  public SolidifyTokenServices(OAuth2ClientDetailsService oAuth2ClientDetailsService, SolidifyTokenStore tokenStore,
          SolidifyAccessTokenConverter accessTokenConverter) {
    this.setTokenStore(tokenStore);
    this.setTokenEnhancer(accessTokenConverter);
    this.setSupportRefreshToken(true);
    this.setReuseRefreshToken(false);
    this.setClientDetailsService(oAuth2ClientDetailsService);
  }

  @Override
  public OAuth2AccessToken refreshAccessToken(String refreshTokenValue, TokenRequest tokenRequest) {
    final OAuth2RefreshToken refreshToken = this.tokenStore.readRefreshToken(refreshTokenValue);
    final OAuth2Authentication authentication = this.tokenStore.readAuthenticationForRefreshToken(refreshToken);

    if (!authentication.isAuthenticated()) {
      throw new AccessDeniedException("Refresh token doesn't hold valid authentication");
    }

    return super.refreshAccessToken(refreshTokenValue, tokenRequest);
  }

  @Override
  public void setTokenStore(TokenStore tokenStore) {
    if (!(tokenStore instanceof SolidifyTokenStore)) {
      throw new IllegalArgumentException("Only SolidifyTokenStore are accepted");
    }
    super.setTokenStore(tokenStore);
    this.tokenStore = (SolidifyTokenStore) tokenStore;
  }
}
