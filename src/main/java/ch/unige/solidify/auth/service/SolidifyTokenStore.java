/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - SolidifyTokenStore.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import java.util.Collection;

import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.model.entity.OAuth2Client;
import ch.unige.solidify.auth.model.entity.TokenSet;
import ch.unige.solidify.auth.repository.AuthUserRepository;
import ch.unige.solidify.auth.repository.OAuth2ClientRepository;
import ch.unige.solidify.auth.repository.TokenSetRepository;
import ch.unige.solidify.auth.util.HashTool;
import ch.unige.solidify.auth.util.StringCipherTools;

// Wait for the first release of the new Spring Authorization Server
// https://spring.io/blog/2020/04/15/announcing-the-spring-authorization-server
@SuppressWarnings({ "deprecation", "squid:CallToDeprecatedMethod" })
@Service
public class SolidifyTokenStore implements TokenStore {

  private final JwtTokenStore jwtTokenStore;
  private final String tokenDatabasePassword;
  private final AuthUserRepository authUserRepository;
  private final TokenSetRepository tokenSetRepository;
  private final OAuth2ClientRepository oAuth2ClientRepository;

  public SolidifyTokenStore(AuthUserRepository authUserRepository,
          TokenSetRepository tokenSetRepository,
          OAuth2ClientRepository oAuth2ClientRepository,
          AuthProperties authProperties,
          SolidifyAccessTokenConverter accessTokenConverter) {
    this.authUserRepository = authUserRepository;
    this.tokenSetRepository = tokenSetRepository;
    this.oAuth2ClientRepository = oAuth2ClientRepository;
    this.jwtTokenStore = new JwtTokenStore(accessTokenConverter);
    this.tokenDatabasePassword = authProperties.getSecurity().getOauth2().getTokenDatabasePassword();
  }

  /**
   * @param clientId the client id to search
   * @return a collection of access tokens
   */
  @Override
  public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
    throw new UnsupportedOperationException();
  }

  /**
   * @param clientId the client id to search
   * @param userName the username to search
   * @return a collection of access tokens
   */
  @Override
  public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
    throw new UnsupportedOperationException();
  }

  /**
   * Retrieve an access token stored against the provided authentication key, if it exists.
   *
   * @param authentication the authentication key for the access token
   * @return the access token or null if there was none
   */
  @Override
  public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
    final OAuth2Client oAuth2Client = this.getOauth2Client(authentication);
    final AuthUser user = this.getUser(authentication);
    final TokenSet tokenSet = this.tokenSetRepository.findByUserAndOAuth2Client(user, oAuth2Client);
    if (tokenSet == null || tokenSet.getAccessToken() == null) {
      return null;
    }
    return this.jwtTokenStore.readAccessToken(this.decryptToken(tokenSet.getAccessToken()));
  }

  /**
   * Read an access token from the store.
   *
   * @param tokenValue The token value.
   * @return The access token to read.
   */
  @Override
  public OAuth2AccessToken readAccessToken(String tokenValue) {
    final TokenSet tokenSet = this.tokenSetRepository.findByAccessTokenHash(HashTool.hash(tokenValue));
    if (tokenSet == null || tokenSet.getAccessToken() == null) {
      throw new InvalidTokenException("Access token not registered");
    }
    return this.jwtTokenStore.readAccessToken(tokenValue);
  }

  /**
   * Read the authentication stored under the specified token value.
   *
   * @param token The token value under which the authentication is stored.
   * @return The authentication, or null if none.
   */
  @Override
  public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
    return this.readAuthentication(token.getValue());
  }

  /**
   * Read the authentication stored under the specified token value.
   *
   * @param tokenValue The token value under which the authentication is stored.
   * @return The authentication, or null if none.
   */
  @Override
  public OAuth2Authentication readAuthentication(String tokenValue) {
    final TokenSet tokenSet = this.tokenSetRepository.findByAccessTokenHash(HashTool.hash(tokenValue));
    if (tokenSet == null) {
      throw new InvalidTokenException("Could not read authentication. Access token not found.");
    }
    return this.jwtTokenStore.readAuthentication(tokenValue);
  }

  /**
   * @param token a refresh token
   * @return the authentication originally used to grant the refresh token
   */
  @Override
  public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
    final TokenSet tokenSet = this.tokenSetRepository.findByRefreshTokenHash(HashTool.hash(token.getValue()));
    if (tokenSet == null) {
      throw new InvalidTokenException("Could not read authentication for refresh token");
    }
    return this.jwtTokenStore.readAuthentication(token.toString());
  }

  /**
   * Read a refresh token from the store.
   *
   * @param tokenValue The value of the token to read.
   * @return The token.
   */
  @Override
  public OAuth2RefreshToken readRefreshToken(String tokenValue) {
    final TokenSet tokenSet = tokenSetRepository.findByRefreshTokenHash(HashTool.hash(tokenValue));
    if (tokenSet == null || tokenSet.getAccessToken() == null) {
      throw new InvalidTokenException("Could not read refresh token");
    }
    return this.jwtTokenStore.readRefreshToken(tokenValue);
  }

  /**
   * Remove an access token from the database.
   *
   * @param token The token to remove from the database.
   */
  @Override
  public void removeAccessToken(OAuth2AccessToken token) {
    final TokenSet tokenSet = this.tokenSetRepository.findByAccessTokenHash(HashTool.hash(token.getValue()));
    if (tokenSet == null) {
      throw new InvalidTokenException("Could not remove access token. Access token not found.");
    }
    tokenSet.setAccessToken(null);
    tokenSet.setAccessTokenHash(null);
    this.tokenSetRepository.save(tokenSet);
  }

  /**
   * Remove an access token using a refresh token. This functionality is necessary so refresh tokens
   * can't be used to create an unlimited number of access tokens.
   *
   * @param refreshToken The refresh token.
   */
  @Override
  public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
    final TokenSet tokenSet = this.tokenSetRepository.findByRefreshTokenHash(HashTool.hash(refreshToken.getValue()));
    if (tokenSet == null) {
      throw new InvalidTokenException("Could not remove access token using refresh token. Refresh token not found.");
    }
    tokenSet.setAccessToken(null);
    tokenSet.setAccessTokenHash(null);
    this.tokenSetRepository.save(tokenSet);
  }

  /**
   * Remove a refresh token from the database.
   *
   * @param token The token to remove from the database.
   */
  @Override
  public void removeRefreshToken(OAuth2RefreshToken token) {
    final TokenSet tokenSet = this.tokenSetRepository.findByRefreshTokenHash(HashTool.hash(token.getValue()));
    if (tokenSet == null) {
      throw new InvalidTokenException("Could not remove refresh token. Refresh token not found");
    }
    tokenSet.setRefreshToken(null);
    tokenSet.setRefreshTokenHash(null);
    this.tokenSetRepository.save(tokenSet);
  }

  /**
   * Store an access token.
   *
   * @param token The token to store.
   * @param authentication The authentication associated with the token.
   */
  @Override
  public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
    final AuthUser user = this.getUser(authentication);
    final OAuth2Client oAuth2Client = this.getOauth2Client(authentication);
    TokenSet tokenSet = this.tokenSetRepository.findByUserAndOAuth2Client(user, oAuth2Client);
    if(tokenSet == null) {
      tokenSet = new TokenSet();
      tokenSet.setUser(user);
      tokenSet.setOAuth2Client(oAuth2Client);
    }
    tokenSet.setAccessTokenHash(HashTool.hash(token.getValue()));
    tokenSet.setAccessToken(this.encryptToken(token.getValue()));
    this.tokenSetRepository.save(tokenSet);
  }

  /**
   * Store the specified refresh token in the database.
   *
   * @param refreshToken The refresh token to store.
   * @param authentication The authentication associated with the refresh token.
   */
  @Override
  public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
    final AuthUser user = this.getUser(authentication);
    final OAuth2Client oAuth2Client = this.getOauth2Client(authentication);
    TokenSet tokenSet = this.tokenSetRepository.findByUserAndOAuth2Client(user, oAuth2Client);
    if(tokenSet == null) {
      tokenSet = new TokenSet();
      tokenSet.setUser(user);
      tokenSet.setOAuth2Client(oAuth2Client);
    }
    tokenSet.setRefreshToken(this.encryptToken(refreshToken.getValue()));
    tokenSet.setRefreshTokenHash(HashTool.hash(refreshToken.getValue()));
    this.tokenSetRepository.save(tokenSet);
  }

  String decryptToken(String token) {
    return StringCipherTools.decrypt(token, this.tokenDatabasePassword);
  }

  String encryptToken(String token) {
    return StringCipherTools.encrypt(token, this.tokenDatabasePassword);
  }

  private AuthUser getUser(OAuth2Authentication authentication) {
    final String externalUid = authentication.getName();
    return this.authUserRepository.findByExternalUid(externalUid)
            .orElseThrow(() -> new IllegalStateException("Cannot find user associated with externalUid " + externalUid));
  }

  private OAuth2Client getOauth2Client(OAuth2Authentication authentication) {
    final String clientId = authentication.getOAuth2Request().getClientId();
    return this.oAuth2ClientRepository.findByClientId(clientId);
  }
}
