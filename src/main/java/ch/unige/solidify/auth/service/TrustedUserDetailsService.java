/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - TrustedUserDetailsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import static ch.unige.solidify.auth.model.AuthConstants.NO_REPLY_PREFIX;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.config.AuthProperties;
import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.TrustedUserDetails;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.model.entity.UserApplicationRole;
import ch.unige.solidify.auth.repository.ApplicationRepository;
import ch.unige.solidify.auth.repository.AuthUserRepository;
import ch.unige.solidify.auth.repository.UserApplicationRoleRepository;

@Service
public class TrustedUserDetailsService implements UserDetailsService {
  private static final Logger log = LoggerFactory.getLogger(TrustedUserDetailsService.class);

  private final Map<AuthProperties.Application, List<AuthProperties.Application.TrustedUser>> trustedUsersMap = new HashMap<>();
  private final AuthUserRepository authUserRepository;
  private final UserApplicationRoleRepository userApplicationRoleRepository;

  public TrustedUserDetailsService(AuthProperties authProperties,
          AuthUserRepository authUserRepository,
          ApplicationRepository applicationRepository,
          UserApplicationRoleRepository userApplicationRoleRepository) {
    this.authUserRepository = authUserRepository;
    this.userApplicationRoleRepository = userApplicationRoleRepository;

    for(final AuthProperties.Application application : authProperties.getApplications()) {
      this.trustedUsersMap.put(application, application.getTrustedUsers());
    }

    for (final AuthProperties.Application applicationFromConfig : trustedUsersMap.keySet()) {
      for(AuthProperties.Application.TrustedUser trustedUser: trustedUsersMap.get(applicationFromConfig)) {
        final Optional<Application> applicationOptional = applicationRepository.findByName(applicationFromConfig.getName());
        if(applicationOptional.isPresent()) {
          this.createTrustedUser(trustedUser.getName(), applicationOptional.get());
        } else {
          throw new IllegalArgumentException("Application from configuration " + applicationFromConfig.getName() + " not registered in database");
        }
      }
    }
  }

  @Override
  public UserDetails loadUserByUsername(String trustedUserName) {
    for (final AuthProperties.Application application : trustedUsersMap.keySet()) {
      for(AuthProperties.Application.TrustedUser trustedUser: trustedUsersMap.get(application)) {
        if (trustedUserName.equals(trustedUser.getName())) {
          return new TrustedUserDetails(trustedUserName, "{noop}" + trustedUser.getPassword());
        }
      }
    }
    throw new UsernameNotFoundException("User " + trustedUserName + " not found");
  }

  private void createTrustedUser(String externalUid, Application application) {
    final String homeOrganization = "unige.ch";
    final String trustedEmail = NO_REPLY_PREFIX + externalUid + "@" + homeOrganization;
    if (!this.authUserRepository.existsByEmail(trustedEmail)) {
      AuthUser trustedUser = new AuthUser();
      trustedUser.setFirstName("Trusted user");
      trustedUser.setLastName(externalUid);
      trustedUser.setEmail(trustedEmail);
      trustedUser.setHomeOrganization(homeOrganization);
      trustedUser.setExternalUid(externalUid);
      ApplicationRole trustedRole = AuthApplicationRole.TRUSTED_CLIENT;
      AuthUser createdUser = this.authUserRepository.save(trustedUser);
      UserApplicationRole userApplicationRole = new UserApplicationRole();
      userApplicationRole.setUser(trustedUser);
      userApplicationRole.setApplicationRole(trustedRole.getResId());
      userApplicationRole.setApplication(application);
      this.userApplicationRoleRepository.save(userApplicationRole);
      log.info("Trusted user '{}' created", createdUser.getResId());
    }
  }
}
