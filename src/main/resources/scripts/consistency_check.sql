# Users without any roles in any applications
SELECT res_id, external_uid, email
FROM auth_db.user
WHERE res_id NOT IN (SELECT user_id FROM auth_db.user_application_role);

# Users registred in administration but not in authorization server
SELECT res_id, external_uid, email
FROM admin_db.user
WHERE external_uid NOT IN (SELECT external_uid FROM auth_db.user);

# User with no synced roles between administration and authorization server
SELECT u.first_name, u.last_name, u.external_uid, u.res_id, uar.application_role, qu.application_role_res_id
FROM auth_db.user auth_u, admin_db.user admin_u, auth_db.user_application_role uar
WHERE auth_u.external_uid = admin_u.external_uid
AND auth_u.res_id = uar.user_id
AND uar.application_role != admin_u.application_role_res_id;
