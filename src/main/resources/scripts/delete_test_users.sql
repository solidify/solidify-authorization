delete from token_set where user_id="user";
delete from token_set where user_id="admin";
delete from token_set where user_id="root";
delete from token_set where user_id="third-party-user";
delete from token_set where user_id="user-with-expired-token";

delete from user_application_role where user_id="user";
delete from user_application_role where user_id="admin";
delete from user_application_role where user_id="root";
delete from user_application_role where user_id="third-party-user";
delete from user_application_role where user_id="user-with-expired-token";

delete from user_login_info where user_id="user";
delete from user_login_info where user_id="admin";
delete from user_login_info where user_id="root";
delete from user_login_info where user_id="third-party-user";
delete from user_login_info where user_id="user-with-expired-token";

delete from user where res_id="user";
delete from user where res_id="admin";
delete from user where res_id="root";
delete from user where res_id="third-party-user";
delete from user where res_id="user-with-expired-token";
