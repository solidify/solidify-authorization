CREATE TABLE `user_application_role` (
  `res_id` varchar(255) NOT NULL,
  `application_role` varchar(255) DEFAULT NULL,
  `creation_when` datetime(6) DEFAULT NULL,
  `creation_who` varchar(255) DEFAULT NULL,
  `last_update_when` datetime(6) DEFAULT NULL,
  `last_update_who` varchar(255) DEFAULT NULL,
  `application_name` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`res_id`),
  UNIQUE KEY `UKftsnxar2tiqtreqjmpdfaetub` (`user_id`,`application_name`),
  KEY `FK8fox2j7y9arprqu76kcbynpwm` (`application_name`),
  CONSTRAINT `FK8fox2j7y9arprqu76kcbynpwm` FOREIGN KEY (`application_name`) REFERENCES `application` (`name`),
  CONSTRAINT `FKov83ojfqvirysy4kxbvrqalne` FOREIGN KEY (`user_id`) REFERENCES `user` (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci

INSERT INTO `user_application_role` (res_id, application_role, creation_when, creation_who, last_update_when, last_update_who, application_name, user_id)
SELECT UUID(), application_role, creation_when, creation_who, last_update_when, last_update_who, 'yareta', res_id FROM user;

ALTER TABLE oauth2client ADD COLUMN `is_mfa` varchar(5) NOT NULL DEFAULT 'false';

ALTER TABLE oauth2client DROP INDEX `UK_by9u8r5prr5dkbavm44ea3dod`;

DROP TABLE token_set;

ALTER TABLE user DROP COLUMN application_role;
