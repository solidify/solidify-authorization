/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthAbstractIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import com.nimbusds.jose.shaded.json.JSONArray;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import reactor.core.publisher.Mono;

import ch.unige.solidify.auth.rest.AuthActionName;

@SpringBootTest(
        classes = TestWithConfigClientConfig.class,
        properties = { "spring.config.use-legacy-processing=true", "spring.cloud.config.enabled:true" }
)
@TestInstance(Lifecycle.PER_CLASS)
public abstract class AuthAbstractIT {

  private final static String ADMIN_MODULE_URL_PROPERTY = "auth.test.dlcm.adminModuleUrl";
  private final static String PREINGEST_MODULE_URL_PROPERTY = "auth.test.dlcm.ingestModuleUrl";

  protected final static String ROLE_USER = "USER";
  protected final static String ROLE_ADMIN = "ADMIN";
  protected final static String ROLE_ROOT = "ROOT";
  protected final static String APPLICATION_DLCM = "dlcm";
  protected final static String APPLICATION_AOU = "aou";
  private final static String USER_ACCESS_TOKEN_PROPERTY = "solidify.oauth2.accesstoken.user";
  private final static String ADMIN_ACCESS_TOKEN_PROPERTY = "solidify.oauth2.accesstoken.admin";
  private final static String ROOT_ACCESS_TOKEN_PROPERTY = "solidify.oauth2.accesstoken.root";
  private final static String USER_WITH_EXPIRED_TOKEN_ACCESS_TOKEN_PROPERTY = "solidify.oauth2.accesstoken.user-with-expired-token";

  protected String adminModuleUrl;
  protected String preingestModuleUrl;

  protected String userAccessToken;
  protected String adminAccessToken;
  protected String rootAccessToken;
  protected String userWithExpiredTokenAccessToken;
  protected WebClient client;
  protected AuthTestProperties authTestProperties;
  protected AuthenticationSimulator authenticationSimulator;

  @Autowired
  Environment env;

  @BeforeAll
  void init() {
    this.authTestProperties = new AuthTestProperties(env);
    this.adminModuleUrl = authTestProperties.getProperty(ADMIN_MODULE_URL_PROPERTY);
    this.preingestModuleUrl = authTestProperties.getProperty(PREINGEST_MODULE_URL_PROPERTY);
    this.userAccessToken = authTestProperties.getProperty(USER_ACCESS_TOKEN_PROPERTY);
    this.adminAccessToken = authTestProperties.getProperty(ADMIN_ACCESS_TOKEN_PROPERTY);
    this.rootAccessToken = authTestProperties.getProperty(ROOT_ACCESS_TOKEN_PROPERTY);
    this.userWithExpiredTokenAccessToken = authTestProperties.getProperty(USER_WITH_EXPIRED_TOKEN_ACCESS_TOKEN_PROPERTY);
    this.authenticationSimulator = new AuthenticationSimulator(authTestProperties, APPLICATION_DLCM);
    this.client = WebClient.create();
    this.initSpecificTest();
  }

  abstract protected void initSpecificTest();

  protected void testProtectedEndpointWithoutCredentials(String urlPath) {
    final ResponseEntity<Void> responseEntity = this.client.get()
            .uri(authenticationSimulator.authorizationUrl + urlPath)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Authorize endpoint should return 401", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  protected String jwt2String(JWT jwt) throws ParseException {
    return jwt.getHeader() + jwt.getJWTClaimsSet().toString();
  }

  protected String forgeToken(SignedJWT jwt, boolean setRoot, boolean setBackInTime) throws ParseException {
    // Original claims map
    final Map<String, Object> claimsMap = jwt.getJWTClaimsSet().getClaims();

    // Forged claims map
    final Map<String, Object> newClaimsMap = new HashMap<>(claimsMap);
    if(setRoot) {
      final JSONArray authorities = (JSONArray) claimsMap.get("authorities");
      authorities.appendElement(ROLE_ROOT);
      newClaimsMap.replace("authorities", authorities);
    }
    if(setBackInTime) {
      newClaimsMap.replace("exp", 0);
    }

    // Forged claims set
    final JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder();
    for (final Map.Entry<String, Object> entry : newClaimsMap.entrySet()) {
      builder.claim(entry.getKey(), entry.getValue());
    }
    final JWTClaimsSet forgedClaimSet = builder.build();

    // Forged JWT
    final Base64URL claimsURL = Base64URL.encode(forgedClaimSet.toString());
    final SignedJWT forgedJwt = new SignedJWT(jwt.getHeader().toBase64URL(), claimsURL, jwt.getSignature());
    System.out.println("Forged token : " + this.jwt2String(forgedJwt));
    return forgedJwt.serialize();
  }

  protected HttpStatus testAccessToken(String url, String oauth2Token) {
    final WebClient client = WebClient.create(url);
    final ResponseEntity<Void> responseEntity = client.get()
            .uri("/")
            .header("Authorization", "Bearer " + oauth2Token)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    return responseEntity.getStatusCode();
  }
  protected HttpStatus testPostAccessToken(String url, String oauth2Token) {
    final WebClient client = WebClient.create(url);
    final ResponseEntity<Void> responseEntity = client.post()
            .uri("/")
            .header("Authorization", "Bearer " + oauth2Token)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    return responseEntity.getStatusCode();
  }

  protected HttpStatus revokeAllTokens(String userPrefix) {
    final WebClient client = WebClient.create(this.adminModuleUrl);
    final ResponseEntity<Void> responseEntity = client
            .post()
            .uri("/users/" + AuthActionName.REVOKE_ALL_TOKENS + "/" + userPrefix + "_externalUid")
            .headers(headers -> headers.setBearerAuth(this.adminAccessToken))
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    return responseEntity.getStatusCode();
  }

}
