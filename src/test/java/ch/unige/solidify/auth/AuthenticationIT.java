/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthenticationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEmptyString.emptyString;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.concurrent.ThreadLocalRandom;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import reactor.core.publisher.Mono;

import ch.unige.solidify.auth.rest.AuthActionName;
import ch.unige.solidify.auth.rest.UrlPath;

class AuthenticationIT extends AuthAbstractIT {

  private final static String TRUSTED_USER_NAME_PROPERTY = "auth.applications[0].trustedusers[0].name";
  private final static String TRUSTED_USER_PASSWORD_PROPERTY = "auth.applications[0].trustedusers[0].password";

  enum RevokeMethod {
    REVOKE_FROM_ADMIN, REVOKE_FROM_USER
  }

  private String trustedUserBadBasicCrendentials;
  private String trustedUserBasicCrendentials;

  @Override
  protected void initSpecificTest() {
    final String trustedUserInfo = authTestProperties.getProperty(TRUSTED_USER_NAME_PROPERTY) + ":"
            + authTestProperties.getProperty(TRUSTED_USER_PASSWORD_PROPERTY);
    this.trustedUserBasicCrendentials = Base64Utils.encodeToString(trustedUserInfo.getBytes(StandardCharsets.UTF_8));
    this.trustedUserBadBasicCrendentials = Base64Utils.encodeToString((trustedUserInfo + "AAAA").getBytes(StandardCharsets.UTF_8));
  }

  @Test
  void testAuthenticationRevokeFromAdmin() throws IOException, ParseException {
    this.testAuthentication(RevokeMethod.REVOKE_FROM_ADMIN);
  }

  @Test
  void testAuthenticationRevokeFromUser() throws IOException, ParseException {
    this.testAuthentication(RevokeMethod.REVOKE_FROM_USER);
  }

  @Test
  void testAuthorizeEndpointWithoutCredentials() {
    final ResponseEntity<Void> responseEntity = this.client.get()
      .uri(authenticationSimulator.authorizationUrl + "/oauth/" + AuthActionName.AUTHORIZE)
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
      assertEquals("302 FOUND should be returned", HttpStatus.FOUND, responseEntity.getStatusCode());
      final String location = responseEntity.getHeaders().getFirst("Location");
      assertEquals("Wrong redirect location", authenticationSimulator.authorizationUrl + UrlPath.AUTH_SHIBLOGIN, location);
  }

  @Test
    void testAuthorizeEndpointWithTrustedCredentials() {
        final ResponseEntity<Void> responseEntity = this.client.get()
            .uri(authenticationSimulator.authorizationUrl +
                     authenticationSimulator.AUTHORIZE_URI_TEMPLATE,
                     authenticationSimulator.STATE,
                     authenticationSimulator.RESPONSE_TYPE,
                     authenticationSimulator.APPROVAL_PROMPT,
                     authenticationSimulator.redirectUri,
                     authenticationSimulator.oAuth2ClientId)
            .header("Authorization", "Basic " + this.trustedUserBasicCrendentials)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("303 SEE_OTHER should be returned", HttpStatus.SEE_OTHER, responseEntity.getStatusCode());
    final String location = responseEntity.getHeaders().getFirst("Location");
    final int index = location.indexOf('?');
    assertNotEquals("Missing query parameters in redirect location", -1, index);
    assertEquals("Wrong redirect location", authenticationSimulator.redirectUri, location.substring(0, index));
    assertThat("Missing code in redirect location", location.substring(index).contains("code="));
  }

  @Test
  void testBadAuthorizationCode() {
    final String authorizationCode = "AAAAAA";
    assertEquals("BAD_REQUEST should be returned", HttpStatus.BAD_REQUEST, authenticationSimulator.getOAuth2Token(authorizationCode).getStatusCode());
  }

  @Test
  void testInfoEndpointWithoutCredentials() {
    final WebClient client = WebClient.create(authenticationSimulator.authorizationUrl);
    final ResponseEntity<Void> responseEntity = client
      .get()
      .uri("/info")
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    assertEquals("OK should be returned", HttpStatus.OK, responseEntity.getStatusCode());
  }

  @Test
  void testNotExistingEndpointWithoutCredentials() {
    this.testProtectedEndpointWithoutCredentials("/afadgagrwz");
  }

  @Test
  void testResourceEndpointWithBadTrustedCredentials() {
    final WebClient client = WebClient.create(this.adminModuleUrl);
    final ResponseEntity<Void> responseEntity = client.get()
      .uri("/")
      .header("Authorization", "Basic " + this.trustedUserBadBasicCrendentials)
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testResourceEndpointWithoutTrustedCredentials() {
    final WebClient client = WebClient.create(this.adminModuleUrl);
    final ResponseEntity<Void> responseEntity = client.get()
      .uri("/users/")
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testResourceEndpointWithTrustedCredentials() {
    final WebClient client = WebClient.create(this.adminModuleUrl);
    final ResponseEntity<Void> responseEntity = client.get()
      .uri("/")
      .header("Authorization", "Basic " + this.trustedUserBasicCrendentials)
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testResourceForbiddenForUserAllowedForAdmin() throws IOException {

    // Request accepted for admin
    String springSessionId = authenticationSimulator.simulateShibbolethLogin(ROLE_ADMIN);
    String authorizationCode = authenticationSimulator.extractAuthorizationCode(authenticationSimulator.getAuthorizationCodeRedirect(springSessionId));
    String jsonString = authenticationSimulator.getOAuth2Token(authorizationCode).getBody();
    String accessToken = authenticationSimulator.getAccessTokenFromJson(jsonString);
    assertEquals("OK should be returned", HttpStatus.OK, this.testAccessToken(this.adminModuleUrl + "/notifications", accessToken));

    // Request refused for user
    springSessionId = authenticationSimulator.simulateShibbolethLogin(ROLE_USER);
    authorizationCode = authenticationSimulator.extractAuthorizationCode(authenticationSimulator.getAuthorizationCodeRedirect(springSessionId));
    jsonString = authenticationSimulator.getOAuth2Token(authorizationCode).getBody();
    accessToken = authenticationSimulator.getAccessTokenFromJson(jsonString);
    assertEquals("FORBIDDEN should be returned", HttpStatus.FORBIDDEN, this.testAccessToken(this.adminModuleUrl + "/notifications", accessToken));
  }

  @Test
  void testTokenEndpointAuthenticationWithBadCredentials() {
    final ResponseEntity<Void> responseEntity = this.client
      .get()
      .uri(authenticationSimulator.authorizationUrl + "/" + AuthActionName.CHECK_TOKEN)
      .header("Authorization", "Basic sfgv5zwbvqb77q")
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testTokenEndpointAuthenticationWithOAuth2Credentials() {
    final ResponseEntity<Void> responseEntity = this.client
      .get()
      .uri(authenticationSimulator.authorizationUrl + "/" + AuthActionName.CHECK_TOKEN)
      .header("Authorization", "Basic " + authenticationSimulator.oAuth2ClientCredentials)
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testCheckTokenEndpointWithoutCredentials() {
    this.testProtectedEndpointWithoutCredentials("/" + AuthActionName.CHECK_TOKEN);
  }

  @Test
  void testTokenEndpointWithoutCredentials() {
    this.testProtectedEndpointWithoutCredentials("/" + AuthActionName.TOKEN);
  }

  private String getRefreshTokenFromJson(String jsonString) throws IOException {
    final JsonNode jwt = new ObjectMapper().readTree(jsonString);
    return jwt.get("refresh_token").textValue();
  }

  private ResponseEntity<String> refreshTokens(String refreshToken, String oAuth2ClientCredentials) {
    return this.client
      .post()
      .uri(authenticationSimulator.authorizationUrl + "/oauth/" + AuthActionName.TOKEN).header("Content-Type", "application/x-www-form-urlencoded")
      .header("Authorization", "Basic " + oAuth2ClientCredentials)
      .body(BodyInserters.fromValue("grant_type=refresh_token&client_id=" + authenticationSimulator.oAuth2ClientId + "&refresh_token=" + refreshToken))
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toEntity(String.class)
      .block();
  }

  private HttpStatus revokeMyTokens(String userToken) {
    final WebClient client = WebClient.create(this.adminModuleUrl);
    final ResponseEntity<Void> responseEntity = client
      .post()
      .uri("/users/" + AuthActionName.REVOKE_MY_TOKENS)
      .headers(headers -> headers.setBearerAuth(userToken))
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    return responseEntity.getStatusCode();
  }

  private void testAuthentication(RevokeMethod revokeMethod) throws IOException, ParseException {
    System.out.println("1) *******************************************");
    System.out.println("Simulate a Shibboleth login on " + UrlPath.AUTH_SHIBLOGIN + " and retrieve the Spring session id");
    final String userPrefix = "bob" + ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE);
    final String springSessionId = authenticationSimulator.simulateShibbolethLogin(userPrefix);
    System.out.println("Spring session id : " + springSessionId);
    System.out.println();
    assertThat(springSessionId, CoreMatchers.not(is(emptyString())));

    System.out.println("2) *******************************************");
    System.out.println("Request OAuth2 authorization code inside the Spring session on " + UrlPath.AUTH_AUTHORIZE);
    System.out.println("First try");
    final String authorizationCode = authenticationSimulator.extractAuthorizationCode(authenticationSimulator.getAuthorizationCodeRedirect(springSessionId));
    System.out.println("Authorization code: " + authorizationCode);
    System.out.println();
    assertThat(authorizationCode, CoreMatchers.not(is(emptyString())));

    System.out.println("Second try with same JSESSIONID, should be redirected to /shiblogin");
    final String authorizationCodeLocation = authenticationSimulator.getAuthorizationCodeRedirect(springSessionId);
    System.out.println();
    assertTrue("Authorization code redirect should end with shiblogin", authorizationCodeLocation.endsWith("shiblogin"));

    System.out.println("\n3) *******************************************");
    System.out.println("Get the OAuth2 token on " + UrlPath.AUTH_TOKEN);
    final String jsonString = authenticationSimulator.getOAuth2Token(authorizationCode).getBody();
    String accessToken = authenticationSimulator.getAccessTokenFromJson(jsonString);
    final String refreshToken = this.getRefreshTokenFromJson(jsonString);
    final ObjectMapper mapper = new ObjectMapper();
    final JsonNode jwt = mapper.readTree(jsonString);
    System.out.println("Access token : " + accessToken);
    System.out.println("Refresh token : " + refreshToken);
    System.out.println("Authorities : " + jwt.get("authorities"));
    System.out.println("Expiration in : " + jwt.get("expires_in"));
    System.out.println("jti : " + jwt.get("jti"));
    System.out.println("scope : " + jwt.get("scope"));
    System.out.println("token_type : " + jwt.get("bearer"));
    System.out.println("user_name : " + jwt.get("user_name"));
    System.out.println("Decoded access token : " + this.jwt2String(JWTParser.parse(accessToken)));
    System.out.println("Decoded refresh token : " + this.jwt2String(JWTParser.parse(accessToken)));
    assertThat(accessToken, CoreMatchers.not(is(emptyString())));
    assertThat(refreshToken, CoreMatchers.not(is(emptyString())));

    System.out.println("\n4) *******************************************");
    System.out.println("Test the OAuth2 access token");
    assertEquals("OK should be returned", HttpStatus.OK, this.testAccessToken(this.adminModuleUrl, accessToken));
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, this.testAccessToken(this.adminModuleUrl, accessToken + "AAAA"));
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, this.testAccessToken(this.adminModuleUrl, this.forgeToken((SignedJWT) JWTParser.parse(accessToken), true, false)));

    System.out.println("\n5) *******************************************");
    System.out.println("Test the OAuth2 refresh token");
    accessToken = this.testRefreshToken(refreshToken);

    System.out.println("\n6) *******************************************");
    System.out.println("Revoke access and refresh tokens");

    switch (revokeMethod) {
      case REVOKE_FROM_USER:
        assertEquals("OK should be returned", HttpStatus.OK, this.revokeMyTokens(accessToken));
        break;
      case REVOKE_FROM_ADMIN:
        assertEquals("OK should be returned", HttpStatus.OK, this.revokeAllTokens(userPrefix));
        assertEquals("NOT_FOUND should be returned", HttpStatus.NOT_FOUND, this.revokeAllTokens(userPrefix + "AAAA"));
        break;
      default:
        throw new IllegalStateException("Unkown revoke method");
    }
    assertEquals("UNAUTHORIZED should be returned", HttpStatus.UNAUTHORIZED, this.testAccessToken(this.adminModuleUrl, jwt.get("access_token").textValue()));
  }

  private String testRefreshToken(String oauth2Token) throws IOException {

    final WebClient client1 = WebClient.create(this.adminModuleUrl);
    final ResponseEntity<Void> responseEntity1 = client1.get()
      .uri("/")
      .header("Authorization", "Bearer " + oauth2Token)
      .retrieve()
      .onStatus(status -> true, clientResponse -> Mono.empty())
      .toBodilessEntity()
      .block();
    assertEquals("Refresh token should not allow to access resource server", HttpStatus.UNAUTHORIZED, responseEntity1.getStatusCode());

    final ResponseEntity<String> responseEntity2 = this.refreshTokens(oauth2Token, authenticationSimulator.oAuth2ClientCredentials);
    assertEquals("Refreshing tokens should be allowed", HttpStatus.OK, responseEntity2.getStatusCode());

    final ResponseEntity<String> responseEntity3 = this.refreshTokens(oauth2Token + "AAA", authenticationSimulator.oAuth2ClientCredentials);
    assertEquals("Refreshing tokens with bad refresh token should be unauthorized", HttpStatus.UNAUTHORIZED, responseEntity3.getStatusCode());

    final ResponseEntity<String> responseEntity4 = this.refreshTokens(oauth2Token, authenticationSimulator.oAuth2ClientCredentials + "AAA");
    assertEquals("Refreshing tokens with bad OAuth2 client credentials should be unauthorized", HttpStatus.UNAUTHORIZED,
            responseEntity4.getStatusCode());

    final ResponseEntity<String> responseEntity5 = this.refreshTokens(oauth2Token, authenticationSimulator.oAuth2ClientCredentials);
    assertEquals("Using two times the same refresh token should be unauthorized", HttpStatus.UNAUTHORIZED, responseEntity5.getStatusCode());

    final String jsonString = responseEntity2.getBody();
    final String accessToken = authenticationSimulator.getAccessTokenFromJson(jsonString);
    final String refreshToken = this.getRefreshTokenFromJson(jsonString);

    assertEquals("The new access token should be valid", HttpStatus.OK, this.testAccessToken(this.adminModuleUrl, accessToken));

    final ResponseEntity<String> responseEntity6 = this.refreshTokens(refreshToken, authenticationSimulator.oAuth2ClientCredentials);
    final String newAccessToken = authenticationSimulator.getAccessTokenFromJson(responseEntity6.getBody());
    assertEquals("The new refresh token should be valid", HttpStatus.OK, responseEntity6.getStatusCode());

    return newAccessToken;

  }
}
