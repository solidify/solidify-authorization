/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthenticationSimulator.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static ch.unige.solidify.auth.AuthAbstractIT.APPLICATION_AOU;
import static ch.unige.solidify.auth.AuthAbstractIT.APPLICATION_DLCM;
import static org.springframework.test.util.AssertionErrors.assertNotEquals;
import static org.springframework.test.util.AssertionErrors.assertNotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import reactor.core.publisher.Mono;

import ch.unige.solidify.auth.rest.AuthActionName;
import ch.unige.solidify.auth.rest.UrlPath;

public class AuthenticationSimulator {
  public static final String AUTHORIZE_URI_TEMPLATE = "/oauth/" + AuthActionName.AUTHORIZE + "?state={STATE}" + "&scope="
          + "&response_type={RESPONSE_TYPE}" + "&approval_prompt={APPROVAL_PROMPT}"
          + "&redirect_uri={REDIRECT_URI}" + "&client_id={CLIENT_ID}";
  public static final String STATE = UUID.randomUUID().toString();
  public static final String RESPONSE_TYPE = "code";
  public static final String APPROVAL_PROMPT = "auto";
  public static final String GRANT_TYPE = "authorization_code";
  public static final String TOKEN_URI_TEMPLATE = "/oauth/" + AuthActionName.TOKEN + "?code={CODE}" + "&state={STATE}" + "&grant_type={GRANT_TYPE}"
          + "&redirect_uri={REDIRECT_URI}";
  public String redirectUri;
  public String oAuth2ClientId;
  public String oAuth2ClientCredentials;
  public String authorizationUrl;
  private WebClient client;

  public AuthenticationSimulator(AuthTestProperties authTestProperties, String applicationName) {
    if(!applicationName.equals(APPLICATION_DLCM) && !applicationName.equals(APPLICATION_AOU)) {
      throw new IllegalArgumentException("Only dlcm and aou applications are supported");
    }
    this.redirectUri = authTestProperties.getProperty("auth.test." + applicationName + ".client.redirectUri");
    this.oAuth2ClientId = authTestProperties.getProperty("auth.test." + applicationName + ".client.clientId");
    String oAuth2ClientSecret = authTestProperties.getProperty("auth.test." + applicationName + ".client.secret");
    this.oAuth2ClientCredentials = Base64Utils.encodeToString((this.oAuth2ClientId + ":" + oAuth2ClientSecret).getBytes(StandardCharsets.UTF_8));
    this.authorizationUrl = authTestProperties.getProperty("auth.test." + applicationName + ".authModuleUrl");
    this.client = WebClient.create();
  }

  protected String simulateShibbolethLogin(String prefix) {
    final WebClient client = WebClient.create(this.authorizationUrl);
    final ResponseEntity<Void> responseEntity = client.get()
            .uri(AUTHORIZE_URI_TEMPLATE, STATE, RESPONSE_TYPE, APPROVAL_PROMPT, this.redirectUri, this.oAuth2ClientId)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    final String springSessionCookie = responseEntity.getHeaders().getFirst("Set-Cookie");
    final int startSessionId = springSessionCookie.indexOf('=') + 1;
    final int endSessionId = springSessionCookie.indexOf(';');
    final String sessionId = springSessionCookie.substring(startSessionId, endSessionId);
    client.get()
            .uri(UrlPath.AUTH_SHIBLOGIN)
            .header("uniqueid", prefix + "_externalUid")
            .header("mail", prefix + "@unige.ch")
            .header("surname", prefix + "_lastName")
            .header("givenname", prefix + "_firstName")
            .header("homeorganization", prefix + "_homeOrganization")
            .header("cookie", "JSESSIONID=" + sessionId)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    return sessionId;
  }

  protected String extractAuthorizationCode(String location) {
    assertNotNull("No location header for extracting authorization code", location);
    final int equalsCharacterIndex = location.indexOf('=');
    final int ampersandCharacterIndex = location.indexOf('&');
    final String errorMessage = "Authorization code absent from location header";
    assertNotEquals(errorMessage, -1, equalsCharacterIndex);
    assertNotEquals(errorMessage, -1, ampersandCharacterIndex);
    return location.substring(equalsCharacterIndex + 1, ampersandCharacterIndex);
  }

  protected String getAccessTokenFromJson(String jsonString) throws IOException {
    final JsonNode jwt = new ObjectMapper().readTree(jsonString);
    return jwt.get("access_token").textValue();
  }

  protected String getAuthorizationCodeRedirect(String springSessionId) {
    final ResponseEntity<Void> responseEntity = this.client
            .get()
            .uri(this.authorizationUrl + AUTHORIZE_URI_TEMPLATE, STATE, RESPONSE_TYPE, APPROVAL_PROMPT, this.redirectUri, this.oAuth2ClientId)
            .cookie("JSESSIONID", springSessionId)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    final String clientRedirect = responseEntity.getHeaders().getFirst("Location");
    System.out.println("Client redirect : " + clientRedirect);
    return clientRedirect;
  }

  protected ResponseEntity<String> getOAuth2Token(String authorizationCode) {
    return this.client
            .post()
            .uri(this.authorizationUrl + TOKEN_URI_TEMPLATE, authorizationCode, STATE, GRANT_TYPE, this.redirectUri)
            .header("Authorization", "Basic " + this.oAuth2ClientCredentials)
            // The authorization server should not be perturbed by any sessionID
            .cookie("JSESSIONID", "AAAAAA")
            .retrieve()
            // Don't throw exceptions on 4xx HTTP error
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toEntity(String.class)
            .block();
  }


}
