/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - AuthorizationEndpointsIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import reactor.core.publisher.Mono;

class AuthorizationEndpointsIT extends AuthAbstractIT {

  @Override
  protected void initSpecificTest() {
    // Do nothing
  }

  @Test
  void testModuleEndpointWithoutCredentials() {
    final ResponseEntity<Void> responseEntity = this.client.get()
            .uri(authenticationSimulator.authorizationUrl + "/")
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Module endpoint should return 401", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testModuleEndpointWithUserCredentials() {
    final ResponseEntity<Void> responseEntity = this.client.get()
            .uri(authenticationSimulator.authorizationUrl + "/")
            .header("Authorization", "Bearer " + this.userAccessToken)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Module endpoint should return 403", HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
  }

  @Test
  void testModuleEndpointWithRootCredentials() {
    final ResponseEntity<Void> responseEntity = this.client.get()
            .uri(authenticationSimulator.authorizationUrl + "/")
            .header("Authorization", "Bearer " + this.rootAccessToken)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Module endpoint should return 200", HttpStatus.OK, responseEntity.getStatusCode());
  }
}
