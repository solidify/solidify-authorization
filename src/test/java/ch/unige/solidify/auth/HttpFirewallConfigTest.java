/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - HttpFirewallConfigTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.firewall.FirewalledRequest;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import ch.unige.solidify.auth.config.HttpFirewallConfig;

class HttpFirewallConfigTest {

  private static final String HEADER_NAME = "lastname";

  private static final String ISO_TEXT_1 = "John Albert";
  private static final String ISO_TEXT_2 = "Jõhn Albert";
  private static final String NON_ISO_TEXT_1 = "JÖhn Albert";
  private static final String NON_ISO_TEXT_2 = "John Älbert";

  @Test
  void strictHttpFirewallHeaderValuesTest() {
    // default Spring HttpFirewall is StrictHttpFirewall
    StrictHttpFirewall httpFirewall = new StrictHttpFirewall();

    this.testValidHeader(httpFirewall, this.getIso88591Text(ISO_TEXT_1));
    this.testValidHeader(httpFirewall, this.getIso88591Text(ISO_TEXT_2));

    this.testInvalidHeader(httpFirewall, this.getIso88591Text(NON_ISO_TEXT_1));
    this.testInvalidHeader(httpFirewall, this.getIso88591Text(NON_ISO_TEXT_2));
    this.testInvalidHeader(httpFirewall, this.getControlChar());
  }

  @Test
  void customizedStrictHttpFirewallHeaderValuesTest() {
    HttpFirewall httpFirewall = new HttpFirewallConfig().getHttpFirewall();

    this.testValidHeader(httpFirewall, this.getIso88591Text(ISO_TEXT_1));
    this.testValidHeader(httpFirewall, this.getIso88591Text(ISO_TEXT_2));
    this.testValidHeader(httpFirewall, this.getIso88591Text(NON_ISO_TEXT_1));
    this.testValidHeader(httpFirewall, this.getIso88591Text(NON_ISO_TEXT_2));

    this.testInvalidHeader(httpFirewall, this.getControlChar());
  }

  private String getIso88591Text(String value) {
    return new String(value.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
  }

  /**
   * Get a String representation of the ASCII control char 2 ("Start of Text")
   *
   * @return
   */
  private String getControlChar() {
    return Character.toString(2);
  }

  private void testValidHeader(HttpFirewall httpFirewall, String headerValue) {
    MockHttpServletRequest request = this.getRequest(headerValue);
    FirewalledRequest firewalledRequest = httpFirewall.getFirewalledRequest(request);
    assertEquals(headerValue, firewalledRequest.getHeader(HEADER_NAME));
  }

  private void testInvalidHeader(HttpFirewall httpFirewall, String headerValue) {
    MockHttpServletRequest request = this.getRequest(headerValue);
    FirewalledRequest firewalledRequest = httpFirewall.getFirewalledRequest(request);
    assertThrows(RequestRejectedException.class, () -> firewalledRequest.getHeader(HEADER_NAME));
  }

  private MockHttpServletRequest getRequest(String headerValue) {
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.setMethod("POST");
    request.addHeader(HEADER_NAME, headerValue);
    return request;
  }
}
