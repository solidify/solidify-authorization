/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - MultiApplicationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.text.ParseException;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.HttpStatus;

import com.nimbusds.jose.shaded.json.JSONArray;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;

class MultiApplicationIT extends AuthAbstractIT {

  @Override
  protected void initSpecificTest() {

  }

  @ParameterizedTest
  @ValueSource(strings = { APPLICATION_DLCM, APPLICATION_AOU })
  void tokenScopeTest(String applicationName) throws IOException, ParseException {
    final String tokenString = this.getToken(applicationName, ROLE_ADMIN);
    final JWT token = JWTParser.parse(tokenString);
    final String scope = (String)((JSONArray)(token.getJWTClaimsSet().getClaims()).get("scope")).get(0);
    assertEquals(applicationName, scope);
  }

  @ParameterizedTest
  @ValueSource(strings = { APPLICATION_DLCM, APPLICATION_AOU })
  void wrongScopeTest(String applicationName) throws IOException {
    final String wrongApplicationName = this.getOtherApplicationName(applicationName);
    final String tokenString = this.getToken(applicationName, ROLE_ADMIN);
    final String correctUrl = authTestProperties.getProperty("auth.test." + applicationName + ".adminModuleUrl");
    final String wrongUrl = authTestProperties.getProperty("auth.test." + wrongApplicationName + ".adminModuleUrl");
    assertEquals(HttpStatus.OK, this.testAccessToken(correctUrl, tokenString));
    assertEquals(HttpStatus.UNAUTHORIZED, this.testAccessToken(wrongUrl, tokenString));
  }

  @ParameterizedTest
  @ValueSource(strings = { APPLICATION_DLCM, APPLICATION_AOU })
  void roleMultiApplicationTest(String applicationName) throws IOException {
    final String otherApplicationName = this.getOtherApplicationName(applicationName);
    final String tokenString = this.getToken(applicationName, ROLE_ROOT);
    final String otherTokenString = this.getToken(otherApplicationName, ROLE_ADMIN);
    final String actionUrl = "/scheduled-tasks/not-existing-task/kill-task";
    final String rootOnlyUrl = authTestProperties.getProperty("auth.test." + applicationName + ".adminModuleUrl") + actionUrl;
    final String rootOnlyUrlOtherApplication = authTestProperties.getProperty("auth.test." + otherApplicationName + ".adminModuleUrl") + actionUrl;
    assertEquals(HttpStatus.NOT_FOUND, this.testPostAccessToken(rootOnlyUrl, tokenString));
    assertEquals(HttpStatus.FORBIDDEN, this.testPostAccessToken(rootOnlyUrlOtherApplication, otherTokenString));
  }

  private String getToken(String applicationName, String role) throws IOException {
    AuthenticationSimulator authenticationSimulator = new AuthenticationSimulator(this.authTestProperties, applicationName);
    String springSessionId = authenticationSimulator.simulateShibbolethLogin(role);
    String authorizationCode = authenticationSimulator.extractAuthorizationCode(authenticationSimulator.getAuthorizationCodeRedirect(springSessionId));
    String jsonString = authenticationSimulator.getOAuth2Token(authorizationCode).getBody();
    return authenticationSimulator.getAccessTokenFromJson(jsonString);
  }

  private String getOtherApplicationName(String applicationName) {
    String otherApplicationName;
    if(applicationName.equals(APPLICATION_DLCM)) {
      otherApplicationName = APPLICATION_AOU;
    } else {
      otherApplicationName = APPLICATION_DLCM;
    }
    return otherApplicationName;
  }

}
