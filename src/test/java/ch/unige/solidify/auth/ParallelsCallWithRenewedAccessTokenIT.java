/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - ParallelsCallWithRenewedAccessTokenIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

/**
 * Run this test with :
 * In Authorization server:
 * - auth.applications[x].oauth2clients[y].accessTokenValiditySeconds = 1
 *   where x corresponds to the DLCM application
 *   and y to the OAuth2 client used by the DLCM application
 * In DLCM:
 * - auth.client.opaqueTokenIntrospectorCacheEnabled = false
 * - auth.client.trustedTokenRefreshDelay = 1
 */

class ParallelsCallWithRenewedAccessTokenIT extends AuthAbstractIT {

  private static final long DELAY_MILLISECONDS = 10;
  private static  final int NB_THREADS = 10;
  private static final int NB_CALLS = 1000;

  private Map<HttpStatus, Integer> results;

  @Override
  protected void initSpecificTest() {
    // Do nothing
  }


  @Test
  void parallelCallsWithAccessTokenCheckedDirectly() throws InterruptedException {
    this.results = new HashMap<>();
    this.runParallelCalls(this::callAdminEndpointGeneratingTrustedCallToAuthServer);
    this.checkTest();
  }

  @Test
  void parallelCallsWithAccessTokenCheckedRemotely() throws InterruptedException {
    this.results = new HashMap<>();
    this.runParallelCalls(this::callPreingestEndpointGeneratingTrustedCallToAdminServer);
    this.checkTest();
  }

  private void callAdminEndpointGeneratingTrustedCallToAuthServer() {
    HttpStatus status = super.revokeAllTokens("NO_ONE");
    this.updateResult(status);
  }

  private void callPreingestEndpointGeneratingTrustedCallToAdminServer() {
    HttpStatus status = this.createDeposit();
    this.updateResult(status);
  }

  private synchronized void updateResult(HttpStatus status) {
    int actualValue = this.results.getOrDefault(status, 0);
    this.results.put(status, actualValue + 1);
  }

  private void checkTest() {
    System.out.println("Result map: " + this.results);
    assertEquals(NB_CALLS, this.results.values().stream().mapToInt(Integer::intValue).sum(), "Not all thread calls have been collected");
    assertFalse(this.results.containsKey(HttpStatus.UNAUTHORIZED), "Some calls have been unauthorized");
  }

  private void runParallelCalls(Runnable runnable) throws InterruptedException {
    final CountDownLatch countDownLatch = new CountDownLatch(NB_CALLS);
    final ExecutorService executorService = Executors.newFixedThreadPool(NB_THREADS);
    for (int i = 0; i < NB_CALLS; i++) {
      executorService.submit(() -> {
        try {
          runnable.run();
          countDownLatch.countDown();
        } catch (Throwable t) {
          t.printStackTrace();
        }
      });
    }
    countDownLatch.await();
  }

  private HttpStatus createDeposit() {
    final WebClient client = WebClient.create(this.preingestModuleUrl);
    try {
      TimeUnit.MILLISECONDS.sleep(DELAY_MILLISECONDS);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();  //set the flag back to true
    }
    final ResponseEntity<Void> response = client.post()
            .uri("/deposits")
            .headers(headers -> headers.setBearerAuth(this.userAccessToken))
            .headers(headers -> headers.setContentType(MediaType.APPLICATION_JSON))
            .body(Mono.just("{}"), String.class)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    return response.getStatusCode();
  }
}
