/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Authorization - Solidify Authorization Server - ResourceEndpointsIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import java.text.ParseException;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import reactor.core.publisher.Mono;

import ch.unige.solidify.auth.rest.UrlPath;

class ResourceEndpointsIT extends AuthAbstractIT {

  @Override
  protected void initSpecificTest() {
    // Do nothing
  }

  @Test
  void testUsersEndpointWithoutCredentials() {
    this.testProtectedEndpointWithoutCredentials(UrlPath.AUTH_USERS);
  }

  @Test
  void testDeleteTokenEndpointWithoutCredentials() {
    final ResponseEntity<Void> responseEntity = this.client.delete()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/toto123" + UrlPath.DELETE_TOKEN)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Authorize endpoint should return 401", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testDeleteTokenEndpointWithAdminToken() {
    final ResponseEntity<Void> responseEntity = this.client.delete()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/toto123" + UrlPath.DELETE_TOKEN)
            .header("Authorization", "Bearer " + this.adminAccessToken)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
            assertEquals("Authorize endpoint should return 401", HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
  }

  @Test
  void testDeleteTokenEndpointWithRootToken() {
    final ResponseEntity<Void> responseEntity = this.client.delete()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/toto123" + UrlPath.DELETE_TOKEN)
            .header("Authorization", "Bearer " + this.rootAccessToken)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Authorize endpoint should return 404", HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
  }

  @Test
  void testDeleteTokenEndpointWithForgedRootToken() throws ParseException {
    final ResponseEntity<Void> responseEntity = this.client.delete()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/toto123" + UrlPath.DELETE_TOKEN)
            .header("Authorization", "Bearer " + this.forgeToken((SignedJWT) JWTParser.parse(this.adminAccessToken), true, false))
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Authorize endpoint should return 401", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }
  @Test
  void testDeleteTokenEndpointWithForgedExpiredToken() throws ParseException {
    final ResponseEntity<Void> responseEntity = this.client.delete()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/toto123" + UrlPath.DELETE_TOKEN)
            .header("Authorization", "Bearer " + this.forgeToken((SignedJWT) JWTParser.parse(this.adminAccessToken), false, true))
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Authorize endpoint should return 401", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }
  @Test
  void testDeleteTokenEndpointExpiredRootToken() {
    final ResponseEntity<Void> responseEntity = this.client.delete()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/toto123" + UrlPath.DELETE_TOKEN)
            .header("Authorization", "Bearer " + this.userWithExpiredTokenAccessToken)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Authorize endpoint should return 401", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }

  @Test
  void testUpdateRoleWithRootToken() {
    final ResponseEntity<Void> responseEntity = this.client.post()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/not_existing_user/change-role")
            .header("Authorization", "Bearer " + this.rootAccessToken)
            .body(Mono.just("USER"),String.class)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Request should return 404", HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
  }
  @Test
  void testUpdateRoleWithUserToken() {
    final ResponseEntity<Void> responseEntity = this.client.post()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/not_existing_user/change-role")
            .header("Authorization", "Bearer " + this.userAccessToken)
            .body(Mono.just("ROOT"),String.class)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Request should return 403", HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
  }

  @Test
  void testUpdateRoleWithoutToken() {
    final ResponseEntity<Void> responseEntity = this.client.post()
            .uri(authenticationSimulator.authorizationUrl + UrlPath.AUTH_USERS + "/not_existing_user/change-role")
            .body(Mono.just("ROOT"),String.class)
            .retrieve()
            .onStatus(status -> true, clientResponse -> Mono.empty())
            .toBodilessEntity()
            .block();
    assertEquals("Request should return 401", HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
  }
}
