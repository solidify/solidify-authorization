/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - UpdateRolePermissionServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.entity.Application;
import ch.unige.solidify.auth.model.entity.AuthUser;
import ch.unige.solidify.auth.permissionservice.UpdateRolePermissionService;
import ch.unige.solidify.auth.service.AuthUserService;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
class UpdateRolePermissionServiceTest {

  private final static String APPLICATION_NAME = "MyApplication";
  private final static String USER_EXTERNAL_UID = "user";
  private final static String USER2_EXTERNAL_UID = "user";

  private final static String ADMIN_EXTERNAL_UID = "admin";
  private final static String ADMIN2_EXTERNAL_UID = "admin2";
  private final static String ROOT_EXTERNAL_UID = "root";
  private final static String ROOT2_EXTERNAL_UID = "root2";

  private final static String TRUSTED_CLIENT_ID = "trustedClient";
  private final static String USER_ROLE_ID = "USER";
  private final static String ADMIN_ROLE_ID = "ADMIN";
  private final static String ROOT_ROLE_ID = "ROOT";
  private final static String TRUSTED_CLIENT_ROLE_ID = "TRUSTED_CLIENT";


  private final AuthUser user = new AuthUser();
  private final AuthUser user2 = new AuthUser();
  private final AuthUser admin = new AuthUser();
  private final AuthUser admin2 = new AuthUser();
  private final AuthUser root = new AuthUser();
  private final AuthUser root2 = new AuthUser();

  private final AuthUser trustedClient = new AuthUser();

  private final Authentication authentication = mock(Authentication.class);
  private final SecurityContext securityContext = mock(SecurityContext.class);

  @Mock
  private AuthUserService authUserService;

  @Mock
  private Application myApplication;

  private UpdateRolePermissionService updateRolePermissionService;

  @BeforeAll
  void setUp() {
    this.updateRolePermissionService = new UpdateRolePermissionService(this.authUserService);
    this.user.setExternalUid(USER_EXTERNAL_UID);
    this.user2.setExternalUid(USER2_EXTERNAL_UID);
    this.admin.setExternalUid(ADMIN_EXTERNAL_UID);
    this.admin2.setExternalUid(ADMIN2_EXTERNAL_UID);
    this.root.setExternalUid(ROOT_EXTERNAL_UID);
    this.root2.setExternalUid(ROOT2_EXTERNAL_UID);
    this.trustedClient.setExternalUid(TRUSTED_CLIENT_ID);
    SecurityContextHolder.setContext(this.securityContext);
    when(myApplication.getName()).thenReturn(APPLICATION_NAME);
    when(this.securityContext.getAuthentication()).thenReturn(this.authentication);
  }
  private void setAuthUserServiceMock() {
    when(this.authUserService.getApplicationFromToken()).thenReturn(myApplication);
    when(this.authUserService.findByExternalUid(USER_EXTERNAL_UID)).thenReturn(this.user);
    when(this.authUserService.findByExternalUid(ADMIN_EXTERNAL_UID)).thenReturn(this.admin);
    when(this.authUserService.findByExternalUid(ADMIN2_EXTERNAL_UID)).thenReturn(this.admin2);
    when(this.authUserService.findByExternalUid(ROOT_EXTERNAL_UID)).thenReturn(this.root);
    when(this.authUserService.findByExternalUid(ROOT2_EXTERNAL_UID)).thenReturn(this.root2);
    when(this.authUserService.findByExternalUid(TRUSTED_CLIENT_ID)).thenReturn(this.trustedClient);
    when(this.authUserService.getApplicationRole(this.user, APPLICATION_NAME)).thenReturn(USER_ROLE_ID);
    when(this.authUserService.getApplicationRole(this.admin, APPLICATION_NAME)).thenReturn(ADMIN_ROLE_ID);
    when(this.authUserService.getApplicationRole(this.admin2, APPLICATION_NAME)).thenReturn(ADMIN_ROLE_ID);
    when(this.authUserService.getApplicationRole(this.root, APPLICATION_NAME)).thenReturn(ROOT_ROLE_ID);
    when(this.authUserService.getApplicationRole(this.root2, APPLICATION_NAME)).thenReturn(ROOT_ROLE_ID);
    when(this.authUserService.getApplicationRole(this.trustedClient, APPLICATION_NAME)).thenReturn(TRUSTED_CLIENT_ROLE_ID);
  }
  @Test
  void rootIsAllowedToUpgradeAdminTest() {
    this.setRoot();
    assertTrue(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN_EXTERNAL_UID, ROOT_ROLE_ID));
  }

  @Test
  void rootIsAllowedToDowngradeRootTest() {
    this.setRoot();
    assertTrue(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT2_EXTERNAL_UID, ADMIN_ROLE_ID));
    assertTrue(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT2_EXTERNAL_UID, USER_ROLE_ID));
  }

  @Test
  void rootIsNotAllowedToDowngradeHisRoleTest() {
    this.setRoot();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, ADMIN_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, USER_ROLE_ID));
  }

  @Test
  void trustedIsNotAllowed() {
    this.setTrustedClient();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, ADMIN_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, USER_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN_EXTERNAL_UID, ROOT_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN_EXTERNAL_UID, USER_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(USER_EXTERNAL_UID, ROOT_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(USER_EXTERNAL_UID, ADMIN_ROLE_ID));
  }

  @Test
  void setToTrustedIsNotAllowed() {
    this.setRoot();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT2_EXTERNAL_UID, TRUSTED_CLIENT_ROLE_ID));
    this.setAdmin();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN2_EXTERNAL_UID, TRUSTED_CLIENT_ROLE_ID));
    this.setUser();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(USER2_EXTERNAL_UID, TRUSTED_CLIENT_ROLE_ID));
  }

  @Test
  void adminIsAllowedToUpgradeUserToAdminTest() {
    this.setAdmin();
    assertTrue(this.updateRolePermissionService.isAllowedToUpdateRole(USER_EXTERNAL_UID, ADMIN_ROLE_ID));
  }

  @Test
  void adminIsAllowedToDowngradeAdminTest() {
    this.setAdmin();
    assertTrue(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN2_EXTERNAL_UID, USER_ROLE_ID));
  }

  @Test
  void adminIsNotAllowedToDowngradeRootTest() {
    this.setAdmin();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, ADMIN_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, USER_ROLE_ID));
  }

  @Test
  void adminIsNotAllowedToUpgradeAdminTest() {
    this.setAdmin();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN2_EXTERNAL_UID, ROOT_ROLE_ID));
  }

  @Test
  void adminIsNotAllowedToUpgradeToRootTest() {
    this.setAdmin();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(USER_EXTERNAL_UID, ROOT_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN2_EXTERNAL_UID, ROOT_ROLE_ID));
  }

  @Test
  void adminIsNotAllowedToDowngradeHisRoleTest() {
    this.setAdmin();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN_EXTERNAL_UID, USER_ROLE_ID));
  }

  @Test
  void userIsNotAllowedTest() {
    this.setUser();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, ADMIN_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ROOT_EXTERNAL_UID, USER_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN_EXTERNAL_UID, ROOT_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN_EXTERNAL_UID, USER_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(USER2_EXTERNAL_UID, ROOT_ROLE_ID));
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(USER2_EXTERNAL_UID, ADMIN_ROLE_ID));
  }

  @Test
  void inexistentApplicationRoleTest() {
    this.setAdmin();
    assertFalse(this.updateRolePermissionService.isAllowedToUpdateRole(ADMIN_EXTERNAL_UID, "not_existing_role"));
  }

  private void setUser() {
    reset(this.authUserService);
    this.setAuthUserServiceMock();
    when(this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.USER)).thenReturn(true);
    doReturn(this.user).when(this.authUserService).getCurrentUser();
  }

  private void setAdmin() {
    reset(this.authUserService);
    this.setAuthUserServiceMock();
    when(this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.ADMIN)).thenReturn(true);
    doReturn(this.admin).when(this.authUserService).getCurrentUser();
  }

  private void setRoot() {
    reset(this.authUserService);
    this.setAuthUserServiceMock();
    when(this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.ROOT)).thenReturn(true);
    doReturn(this.root).when(this.authUserService).getCurrentUser();
  }
  private void setTrustedClient() {
    reset(this.authUserService);
    this.setAuthUserServiceMock();
    doReturn(this.trustedClient).when(this.authUserService).getCurrentUser();
    when(this.authUserService.currentUserHasApplicationRole(AuthApplicationRole.TRUSTED_CLIENT)).thenReturn(true);
  }
}
